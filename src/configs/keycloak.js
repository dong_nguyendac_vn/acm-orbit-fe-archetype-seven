import Keycloak from 'keycloak-js';

const keycloak = Keycloak({
  url: 'http://localhost:8180/auth',
  realm: 'orbit',
  clientId: 'ops-react',
});

export default keycloak;
