import { TeamOutlined } from '@ant-design/icons';

const PartnerMenu = [
  {
    key: 'partner',
    label: 'Partner',
    icon: TeamOutlined,
    children: [
      {
        key: 'partner-list',
        path: '/partners',
        label: 'Partner List',
        // permission: {
        //   type: 'some',
        //   values: ['view:block-devices'],
        // },
      },
    ],
  },
];

export default PartnerMenu;
