import { HomeOutlined } from '@ant-design/icons';
import ComponentMenus from './component.menus';
import PartnerMenus from './partner.menus';

const AppMenu = [
  {
    key: 'home',
    path: '/',
    label: 'Home',
    icon: HomeOutlined,
  },
  ...PartnerMenus,
];

if (process.env.NODE_ENV) {
  AppMenu.push({ ...ComponentMenus });
}
export default AppMenu;
