const ComponentMenus = [
  {
    key: 'components',
    label: 'Components',
    icon: 'ion-bag',
    children: [
      {
        key: 'components_template',
        label: 'Template',
        children: [
          {
            key: 'components_template_breadcrumb',
            path: '/components/templates',
            label: 'Search Template',
          },
        ],
      },
      {
        key: 'components_general',
        label: 'General',
        children: [
          {
            key: 'components_general_button',
            path: '/components/general/button',
            label: 'Button',
          },
        ],
      },
      {
        key: 'components_data_entry',
        label: 'Data Entry',
        children: [
          {
            key: 'components_data_entry_checkbox',
            path: '/components/data-entry/checkbox',
            label: 'Checkbox',
          },
          {
            key: 'components_data_entry_card',
            path: '/components/data-entry/card',
            label: 'Card',
          },
        ],
      },
      {
        key: 'components_data_display',
        label: 'Data Display',
        children: [
          {
            key: 'components_data_display_tooltip',
            path: '/components/data-display/tooltip',
            label: 'Tooltip',
          },
        ],
      },
      {
        key: 'components_feedback',
        label: 'Feedback',
        children: [
          {
            key: 'components_feedback_notification',
            path: '/components/feedback/notification',
            label: 'Notification',
          },
          {
            key: 'components_feedback_modal',
            path: '/components/feedback/modal',
            label: 'Modal',
          },
        ],
      },
      {
        key: 'components_navigation',
        label: 'Navigation',
        children: [
          {
            key: 'components_navigation_breadcrumb',
            path: '/components/navigation/breadcrumb',
            label: 'Breadcrumb',
          },
        ],
      },
    ],
  },
];

export default ComponentMenus;
