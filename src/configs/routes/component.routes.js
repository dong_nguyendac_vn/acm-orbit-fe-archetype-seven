import { lazy } from 'react';

const ComponentRoutes = [
  {
    path: '/components/data-entry/checkbox',
    breadCrumb: 'Checkbox',
    component: lazy(() => import('screens/Components/Data/Entry/Checkbox')),
  },
  {
    path: '/components/data-entry/card',
    breadCrumb: 'Card',
    component: lazy(() => import('screens/Components/Data/Entry/Card')),
  },
  {
    path: '/components/navigation/breadcrumb',
    breadCrumb: 'Breadcrumb',
    component: lazy(() => import('screens/Components/Navigation/Breadcrumb')),
  },
  {
    path: '/components/templates',
    exact: true,
    breadCrumb: 'Search Template',
    component: lazy(() => import('containers/screens/Components/Template/Search.container')),
  },
  {
    path: '/components/templates/:id',
    breadCrumb: 'Edit Template',
    component: lazy(() => import('screens/Components/Template/Edit')),
  },
  {
    path: '/components/general/button',
    breadCrumb: 'Button',
    component: lazy(() => import('screens/Components/General/Button')),
  },
  {
    path: '/components/data-display/tooltip',
    breadCrumb: 'Tooltip',
    component: lazy(() => import('screens/Components/Data/Display/Tooltip')),
  },
  {
    path: '/components/feedback/notification',
    breadCrumb: 'Notification',
    component: lazy(() => import('screens/Components/Feedback/Notification')),
  },
  {
    path: '/components/feedback/modal',
    breadCrumb: 'Modal',
    component: lazy(() => import('screens/Components/Feedback/Modal')),
  },
];

export default ComponentRoutes;
