import { lazy } from 'react';

const PartnerRoutes = [
  {
    path: '/partners',
    breadCrumb: 'Partner List',
    component: lazy(() => import('containers/screens/Partner/Search.container')),
  },
];

export default PartnerRoutes;
