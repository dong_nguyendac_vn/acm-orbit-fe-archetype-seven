import { lazy } from 'react';
import ComponentRoutes from './component.routes';
import PartnerRoutes from './partner.routes';

const AppRouters = [
  {
    exact: true,
    path: '/',
    component: lazy(() => import('screens/Home')),
  },
  ...PartnerRoutes,
];

if (process.env.NODE_ENV) {
  AppRouters.push({ ...ComponentRoutes });
}

export default AppRouters;
