const DASHBOARD_ROUTE_PREFIX = '/dashboard';

const EXCEPTION_TYPE_CONFIG = {
  404: {
    title: '404',
    description: 'Sorry, the page you visited does not exist.',
  },
  500: {
    title: '500',
    description: 'Sorry, the server is wrong.',
  },
};
export { DASHBOARD_ROUTE_PREFIX, EXCEPTION_TYPE_CONFIG };
