import { applyMiddleware, compose, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import { routerMiddleware } from 'connected-react-router/immutable';
import storage from 'redux-persist/es/storage';
import history from 'utils/history';
import immutableTransform from 'redux-persist-transform-immutable';
import rootSaga from '../saga';
import rootReducer from '../reducer';
import middleware, { sagaMiddleware } from '../middleware';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['app'],
  transforms: [immutableTransform()],
};

const finalReducer = persistReducer(persistConfig, rootReducer);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore(preloadedState) {
  const store = createStore(
    finalReducer,
    preloadedState,
    composeEnhancers(applyMiddleware(routerMiddleware(history), ...middleware)),
  );
  const persistor = persistStore(store);

  sagaMiddleware.run(rootSaga);
  return { persistor, store };
}
