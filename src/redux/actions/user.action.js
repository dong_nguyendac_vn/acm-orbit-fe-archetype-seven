import { createActions } from 'redux-actions';
import keyMirror from 'keymirror';

export const ActionTypes = keyMirror({
  USER_LOAD_INFO: undefined,
  USER_LOAD_INFO_SUCCESS: undefined,
  USER_LOAD_INFO_FAILURE: undefined,
  USER_LOGOUT: undefined,
});

const { userLoadInfo, userLogout } = createActions({
  [ActionTypes.USER_LOAD_INFO]: param => ({ ...param }),
  [ActionTypes.USER_LOGOUT]: param => ({ ...param }),
});

export default { userLoadInfo, userLogout };
