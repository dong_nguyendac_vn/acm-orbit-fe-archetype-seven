import { createActions } from 'redux-actions';
import keyMirror from 'keymirror';

export const ActionTypes = keyMirror({
  TEMPLATE_SHOW_DETAIL: undefined,
});

const { templateShowDetail } = createActions({
  [ActionTypes.TEMPLATE_SHOW_DETAIL]: param => ({ ...param }),
});

export default { templateShowDetail };
