import { createActions } from 'redux-actions';
import keyMirror from 'keymirror';

export const ActionTypes = keyMirror({
  APP_OPEN_MENU: undefined,
  APP_SELECT_MENU: undefined,
  APP_COLLAPSE_MENU: undefined,
});

const { appOpenMenu, appSelectMenu, appCollapseMenu } = createActions({
  [ActionTypes.APP_OPEN_MENU]: param => ({ ...param }),
  [ActionTypes.APP_SELECT_MENU]: param => ({ ...param }),
  [ActionTypes.APP_COLLAPSE_MENU]: param => ({ ...param }),
});
export default { appOpenMenu, appSelectMenu, appCollapseMenu };
