import { createActions } from 'redux-actions';
import keyMirror from 'keymirror';

export const ActionTypes = keyMirror({
  PARTNER_FETCH: undefined,
  PARTNER_FETCH_SUCCESS: undefined,
  PARTNER_FETCH_FAILURE: undefined,
  PARTNER_UPDATE: undefined,
  PARTNER_UPDATE_SUCESS: undefined,
  PARTNER_UPDATE_FAILURE: undefined,
});

const { partnerFetch, partnerUpdate } = createActions({
  [ActionTypes.PARTNER_FETCH]: param => ({ ...param }),
  [ActionTypes.PARTNER_UPDATE]: param => ({ ...param }),
});
export default { partnerFetch, partnerUpdate };
