import { handleActions } from 'redux-actions';
import { fromJS } from 'immutable';
import { ActionTypes } from '../actions/user.action';

const initialState = fromJS({
  profile: {
    id: undefined,
    userName: undefined,
  },
});

const actions = {
  [ActionTypes.USER_LOAD_INFO_SUCCESS]: (state, { payload }) => {
    return state.set('profile', payload);
  },
};

export default handleActions(actions, initialState);
