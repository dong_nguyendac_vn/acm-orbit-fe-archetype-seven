import { combineReducers } from 'redux';
import user from './user.reducer';
import router from './router.reducer';
import partner from './partner.reducer';
import app from './app.reducer';

export default combineReducers({ app, router, user, partner });
