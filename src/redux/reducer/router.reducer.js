import { connectRouter } from 'connected-react-router/immutable';
import history from 'utils/history';

export default connectRouter(history);
