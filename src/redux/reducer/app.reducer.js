import { handleActions } from 'redux-actions';
import { fromJS } from 'immutable';
import { ActionTypes } from '../actions/app.action';

const initialState = fromJS({
  collapsedMenu: window.innerWidth > 1220,
  currentMenuKey: 'home',
  openMenuKeys: fromJS([]),
});

const actions = {
  [ActionTypes.APP_OPEN_MENU]: (state, { payload }) => {
    const { openKeys } = payload;
    return state.set('openMenuKeys', fromJS(openKeys));
  },
  [ActionTypes.APP_SELECT_MENU]: (state, { payload }) => {
    const { selected } = payload;
    return state.set('currentMenuKey', selected);
  },
  [ActionTypes.APP_COLLAPSE_MENU]: state => {
    const collapsedMenu = state.get('collapsedMenu');
    return state.set('collapsedMenu', !collapsedMenu);
  },
};

export default handleActions(actions, initialState);
