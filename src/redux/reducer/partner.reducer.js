import { handleActions } from 'redux-actions';
import { fromJS } from 'immutable';
import { ActionTypes } from '../actions/partner.action';

const initialState = fromJS({
  partners: {
    pages: {},
    data: [],
  },
});

const actions = {
  [ActionTypes.PARTNER_FETCH_SUCCESS]: (state, { payload }) => {
    return state.set('partners', payload);
  },
};

export default handleActions(actions, initialState);
