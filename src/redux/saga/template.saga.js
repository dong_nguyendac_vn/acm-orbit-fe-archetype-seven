import { all, takeLatest } from 'redux-saga/effects';
import { ActionTypes } from 'redux/actions/template.action';
import history from 'utils/history';
import { DASHBOARD_ROUTE_PREFIX } from 'configs/config';

export function* showDetail({ payload }) {
  const { id } = payload;
  yield history.push(`${DASHBOARD_ROUTE_PREFIX}/components/templates/${id}`);
}

export default function* root() {
  yield all([takeLatest(ActionTypes.TEMPLATE_SHOW_DETAIL, showDetail)]);
}
