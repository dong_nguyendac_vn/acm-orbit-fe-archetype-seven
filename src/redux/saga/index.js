import { all, fork } from 'redux-saga/effects';

import template from './template.saga';
import user from './user.saga';
import partner from './partner.saga';

export default function* rootSaga() {
  yield all([fork(template), fork(user), fork(partner)]);
}
