import { all, takeLatest, call, put } from 'redux-saga/effects';
import { ActionTypes } from 'redux/actions/user.action';
import { request } from 'utils/client';

export function* loadInfo() {
  try {
    const payload = { method: 'GET' };
    const response = yield call(request, '/users', payload);
    yield put({
      type: ActionTypes.USER_LOAD_INFO_SUCCESS,
      payload: response,
    });
  } catch (err) {
    yield put({
      type: ActionTypes.USER_LOAD_INFO_FAILURE,
      payload: err,
    });
  }
}

export default function* root() {
  yield all([takeLatest(ActionTypes.USER_LOAD_INFO, loadInfo)]);
}
