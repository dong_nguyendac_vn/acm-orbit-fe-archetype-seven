import { all, takeLatest, call, put } from 'redux-saga/effects';
import { ActionTypes } from 'redux/actions/partner.action';
import { request } from 'utils/client';

export function* fetchPartner({ param }) {
  try {
    const payload = { method: 'POST', ...param };
    const response = yield call(request, '/partners?action=search', payload);
    yield put({
      type: ActionTypes.PARTNER_FETCH_SUCCESS,
      payload: response,
    });
  } catch (err) {
    yield put({
      type: ActionTypes.PARTNER_FETCH_FAILURE,
      payload: err,
    });
  }
}

export default function* root() {
  yield all([takeLatest(ActionTypes.PARTNER_FETCH, fetchPartner)]);
}
