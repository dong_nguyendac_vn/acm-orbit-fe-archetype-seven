import React, { PureComponent } from 'react';
import { FormCurrencySearch } from 'components/group/Form/Currency';
import { TableCurrencyModal } from 'components/group/Table/Currency';
import { ACMPageContent, ACMSearchCriteria, ACMCard, ACMTableContent } from 'components/basic/ACM';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Currency.screen.module.less';

const dataSource = [
  {
    code: 'VND',
    name: 'Vietnam Dong',
  },
  {
    code: 'USD',
    name: 'US Dollar',
  },
];

class CurrencyScreen extends PureComponent {
  onSubmit = () => {
    console.log('called');
  };

  render() {
    const { popup, className } = this.props;
    const { onPopupCallBack } = popup;
    const clsString = classNames(styles.currency__screen__modal, className);
    return (
      <ACMPageContent className={clsString}>
        <ACMSearchCriteria>
          <FormCurrencySearch onFinish={this.onSubmit} />
        </ACMSearchCriteria>
        <ACMTableContent>
          <ACMCard>
            <TableCurrencyModal dataSource={dataSource} onPopupCallBack={onPopupCallBack} />
          </ACMCard>
        </ACMTableContent>
      </ACMPageContent>
    );
  }
}

CurrencyScreen.defaultProps = {
  popup: null,
  className: undefined,
};

CurrencyScreen.propTypes = {
  className: PropTypes.string,
  popup: PropTypes.shape({
    onPopupCallBack: PropTypes.func,
  }),
};

export default CurrencyScreen;
