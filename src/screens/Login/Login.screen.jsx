import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormLogin } from 'components/group/Form/Login';

class LoginScreen extends PureComponent {
  onClickLogin = () => {
    const { login } = this.props;
    login();
  };

  render() {
    return (
      <div className="login-screen">
        <FormLogin onSubmit={this.onClickLogin} />
      </div>
    );
  }
}
LoginScreen.propTypes = {
  login: PropTypes.func.isRequired,
};
export default LoginScreen;
