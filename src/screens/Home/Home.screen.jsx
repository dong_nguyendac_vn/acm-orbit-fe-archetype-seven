import React, { PureComponent } from 'react';
import { ACMCard, ACMTypography, ACMPageContent } from 'components/basic/ACM';

const { ACMTitle } = ACMTypography;
class HomeScreen extends PureComponent {
  render() {
    return (
      <div className="home-screen">
        <ACMPageContent>
          <ACMCard>
            <ACMTitle> Welcome</ACMTitle>
          </ACMCard>
        </ACMPageContent>
      </div>
    );
  }
}
HomeScreen.propTypes = {};

export default HomeScreen;
