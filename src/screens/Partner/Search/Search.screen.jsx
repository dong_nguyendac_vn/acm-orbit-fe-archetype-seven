import React, { PureComponent } from 'react';
import { MainScreenHeaderContainer } from 'containers/group';
import classNames from 'classnames';
import { ACMPageContent, ACMSearchCriteria, ACMCard, ACMTableContent } from 'components/basic/ACM';
import { FormPartnerSearch } from 'components/group/Form/Partner';
import { TablePartnerPage } from 'components/group/Table/Partner';
import PropTypes from 'prop-types';
import styles from './Search.screen.module.less';

class PartnerSearchScreen extends PureComponent {
  onSubmit = e => {
    e.preventDefault();
  };

  openDetail = () => {};

  render() {
    const { partner } = this.props;
    const { data, page } = partner;
    const pagination = {
      total: (page && page.totalElements) || 0,
      current: (page && page.currentPage + 1) || 1,
    };
    const clsString = classNames(styles.partner_search_screen);
    return (
      <div className={clsString}>
        <MainScreenHeaderContainer title="Partner" />
        <ACMPageContent>
          <ACMSearchCriteria>
            <FormPartnerSearch onFinish={this.onSubmit} />
          </ACMSearchCriteria>
          <ACMTableContent>
            <ACMCard>
              <TablePartnerPage dataSource={data} page={pagination} onEdit={this.openDetail} />
            </ACMCard>
          </ACMTableContent>
        </ACMPageContent>
      </div>
    );
  }
}

PartnerSearchScreen.defaultProps = {};

PartnerSearchScreen.propTypes = {
  partner: PropTypes.shape({
    data: PropTypes.arrayOf(PropTypes.object),
    page: PropTypes.shape({
      totalPages: PropTypes.number,
      currentPage: PropTypes.number,
      totalElements: PropTypes.number,
    }),
  }).isRequired,
};

export default PartnerSearchScreen;
