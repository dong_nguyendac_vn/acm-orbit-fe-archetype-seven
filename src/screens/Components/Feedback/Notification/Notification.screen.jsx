import React, { PureComponent } from 'react';
import { ACMPageContent } from 'components/basic/ACM';
import NotificationComponentBasic from './Components/Basic';

class FeedbackNotificationScreen extends PureComponent {
  render() {
    return (
      <ACMPageContent>
        <div className="acm-checkbox-screen">
          <NotificationComponentBasic />
        </div>
      </ACMPageContent>
    );
  }
}
FeedbackNotificationScreen.propTypes = {};
export default FeedbackNotificationScreen;
