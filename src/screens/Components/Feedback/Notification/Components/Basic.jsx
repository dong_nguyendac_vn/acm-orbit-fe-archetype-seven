import React, { memo } from 'react';
import { ACMNotification, ACMButton } from 'components/basic/ACM';
import MainTemplateCard from 'components/group/MainTemplateCard';
import styles from './Basic.module.less';

const code = `
    import { ACMNotification, ACMButton } from 'components/basic/ACM';

    const openNotification = () => {
      const { open } = ACMNotification;
      open({
        key: 'acm__demo__notification',
        title: 'Notification title',
        description:
          'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
      });
    };

    const closeNotification = () => {
      const { close } = ACMNotification;
      close('acm__demo__notification');
    };

    const destroyNotification = () => {
      const { destroy } = ACMNotification;
      destroy();
    };

    ReactDOM.render(
      <div>
        <ACMButton onClick={openNotification}>Open</ACMButton>
        <ACMButton onClick={closeNotification}>Close</ACMButton>
        <ACMButton onClick={destroyNotification}>Destroy</ACMButton>
      </div>,
      mountNode,
    );
`;

const openNotification = () => {
  const { open } = ACMNotification;
  open({
    key: 'acm__demo__notification',
    title: 'Notification title',
    description:
      'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
  });
};

const closeNotification = () => {
  const { close } = ACMNotification;
  close('acm__demo__notification');
};

const destroyNotification = () => {
  const { destroy } = ACMNotification;
  destroy();
};
const NotificationComponentBasic = () => {
  return (
    <MainTemplateCard code={code} title="Notification">
      <div className={styles.notification__basic__demo}>
        <ACMButton onClick={openNotification}>Open</ACMButton>
        <ACMButton onClick={closeNotification}>Close</ACMButton>
        <ACMButton onClick={destroyNotification}>Destroy</ACMButton>
      </div>
    </MainTemplateCard>
  );
};

NotificationComponentBasic.propTypes = {};

export default memo(NotificationComponentBasic);
