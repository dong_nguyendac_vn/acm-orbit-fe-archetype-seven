import React, { PureComponent } from 'react';
import { ACMPageContent, ACMRow, ACMCol } from 'components/basic/ACM';
import ComponentMessage from './Components/Message';
import ComponentForm from './Components/Form';

class FeedbackModalScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleMessage: false,
      visibleForm: false,
    };
  }

  onOpenHandle = () => {
    this.setState({ visibleMessage: true });
  };

  onCloseHandle = () => {
    this.setState({ visibleMessage: false });
  };

  onOpenHandleForm = () => {
    this.setState({ visibleForm: true });
  };

  onCloseHandleForm = () => {
    this.setState({ visibleForm: false });
  };

  render() {
    const { visibleMessage, visibleForm } = this.state;
    return (
      <ACMPageContent>
        <div className="acm-modal-screen">
          <ACMRow gutter={[16, 16]}>
            <ACMCol span={8}>
              <ComponentMessage
                visible={visibleMessage}
                onOpenModal={this.onOpenHandle}
                onCloseModal={this.onCloseHandle}
              />
            </ACMCol>
            <ACMCol span={8}>
              <ComponentForm
                visible={visibleForm}
                onOpenModal={this.onOpenHandleForm}
                onCloseModal={this.onCloseHandleForm}
              />
            </ACMCol>
          </ACMRow>
        </div>
      </ACMPageContent>
    );
  }
}
FeedbackModalScreen.propTypes = {};
export default FeedbackModalScreen;
