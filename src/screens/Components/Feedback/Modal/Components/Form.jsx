import React, { memo } from 'react';
import { ACMModal, ACMButton } from 'components/basic/ACM';
import MainTemplateCard from 'components/group/MainTemplateCard';
import PropTypes from 'prop-types';
import { FormTemplateData } from 'components/group/Form/Template';
import styles from './Message.module.less';

const code = `
  import { ACMModal, ACMButton }  from 'components/basic/ACM';
  import { FormTemplateData } from 'components/group/Form/Template';
  const { DataModal } = ACMModal;

  class ModalExample extends React.Component {
    state = { visible: false };

    showModal = () => {
      this.setState({
        visible: true,
      });
    };

    hideModal = () => {
      this.setState({
        visible: false,
      });
    };

    render() {
      return (
        <div>
          <ACMButton type="primary" onClick={this.showModal}>
            Modal
          </ACMButton>
          <DataModal
          visible={visible}
          title="Edit Form"
          onCancel={this.hideModal}
          >
            <FormTemplateCreate  onFinish={this.hideModal} onCancel={this.hideModal}/>
          </DataModal>
      );
    }
  }

  ReactDOM.render(
    <div>
      <ModalExample />
    </div>,
    mountNode,
  );
`;
const { DataModal } = ACMModal;
const ComponentForm = ({ onOpenModal, visible, onCloseModal }) => {
  return (
    <MainTemplateCard code={code} title="Form Modal">
      <div className={styles.modal__basic__demo}>
        <ACMButton onClick={onOpenModal}>Open Form Modal</ACMButton>
      </div>
      <DataModal visible={visible} title="Edit Form" onCancel={onCloseModal}>
        <FormTemplateData onFinish={onCloseModal} onCancel={onCloseModal} />
      </DataModal>
    </MainTemplateCard>
  );
};
ComponentForm.defaultProps = {
  onOpenModal: () => {},
  onCloseModal: () => {},
  visible: false,
};

ComponentForm.propTypes = {
  onOpenModal: PropTypes.func,
  onCloseModal: PropTypes.func,
  visible: PropTypes.bool,
};

export default memo(ComponentForm);
