import React, { memo } from 'react';
import { ACMModal, ACMButton } from 'components/basic/ACM';
import MainTemplateCard from 'components/group/MainTemplateCard';
import PropTypes from 'prop-types';
import styles from './Message.module.less';

const code = `
  import { ACMModal, ACMButton }  from 'components/basic/ACM';
  const { MessageModal } = ACMModal;

  class ModalExample extends React.Component {
    state = { visible: false };

    showModal = () => {
      this.setState({
        visible: true,
      });
    };

    hideModal = () => {
      this.setState({
        visible: false,
      });
    };

    render() {
      return (
        <div>
          <ACMButton type="primary" onClick={this.showModal}>
            Modal
          </ACMButton>
          <MessageModal
            visible={visible}
            title="Confirm Message"
            type="primary"
           cancel={{ title: 'Cancel', onClick: this.hideModal }}
        ok={{ title: 'Done', onClick: this.hideModal }}
            message="Are you sure you want to delete ‘Lorum Ipsum (ID) - Type’ from Store A Hierarchy?"
          />
      );
    }
  }

  ReactDOM.render(
    <div>
      <ModalExample />
    </div>,
    mountNode,
  );
`;
const { MessageModal } = ACMModal;
const ComponentMessage = ({ onOpenModal, visible, onCloseModal }) => {
  return (
    <MainTemplateCard code={code} title="Message Modal">
      <div className={styles.modal__basic__demo}>
        <ACMButton onClick={onOpenModal}>Open Message Modal</ACMButton>
      </div>
      <MessageModal
        visible={visible}
        title="Confirm Message"
        cancel={{ title: 'Cancel', onClick: onCloseModal }}
        ok={{ title: 'Done', onClick: onCloseModal }}
        message="Are you sure you want to delete ‘Lorum Ipsum (ID) - Type’ from Store A Hierarchy?"
      />
    </MainTemplateCard>
  );
};
ComponentMessage.defaultProps = {
  onOpenModal: () => {},
  onCloseModal: () => {},
  visible: false,
};

ComponentMessage.propTypes = {
  onOpenModal: PropTypes.func,
  onCloseModal: PropTypes.func,
  visible: PropTypes.bool,
};

export default memo(ComponentMessage);
