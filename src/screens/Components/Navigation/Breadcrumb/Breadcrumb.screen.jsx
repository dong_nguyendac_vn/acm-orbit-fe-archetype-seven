import React, { PureComponent } from 'react';
import { ACMPageContent } from 'components/basic/ACM';
import NavigationBreadcrumbBasic from './Components/Basic';

class NavigationBreadcrumbScreen extends PureComponent {
  render() {
    return (
      <ACMPageContent>
        <div className="acm-breadcrumb-screen">
          <NavigationBreadcrumbBasic />
        </div>
      </ACMPageContent>
    );
  }
}
NavigationBreadcrumbScreen.propTypes = {};

export default NavigationBreadcrumbScreen;
