import React, { memo } from 'react';
import { ACMBreadcrumb } from 'components/basic/ACM';
import MainTemplateCard from 'components/group/MainTemplateCard';

const code = `
  import { ACMBreadcrumb } from "components/basic/ACM";
  const breadcrumbs = [
    {
      key: 'home',
      breadcrumb: 'Home',
      match: {
        url: '/dashboard',
      },
    },
    {
      key: 'application_list',
      breadcrumb: 'Application List',
      match: {
        url: '/dashboard/application-list',
      },
    },
    {
      key: 'application',
      breadcrumb: 'Application',
      match: {
        url: '/dashboard/application',
      },
    },
  ];

  ReactDOM.render(<ACMBreadcrumb breadcrumbs={breadcrumbs}/>, mountNode);
`;
const breadcrumbs = [
  {
    key: 'home',
    breadcrumb: 'Home',
    match: {
      url: '/dashboard',
    },
  },
  {
    key: 'application_list',
    breadcrumb: 'Application List',
    match: {
      url: '/dashboard/application-list',
    },
  },
  {
    key: 'application',
    breadcrumb: 'Application',
    match: {
      url: '/dashboard/application',
    },
  },
];
const NavigationBreadcrumbBasic = () => {
  return (
    <MainTemplateCard code={code} title="Basic Breadcrumb">
      <ACMBreadcrumb breadcrumbs={breadcrumbs} />
    </MainTemplateCard>
  );
};

NavigationBreadcrumbBasic.propTypes = {};

export default memo(NavigationBreadcrumbBasic);
