import React, { memo } from 'react';
import { ACMButton, ACMIcon } from 'components/basic/ACM';
import { SearchOutlined } from '@ant-design/icons';
import MainTemplateCard from 'components/group/MainTemplateCard';
import styles from './Action.module.less';

const code = `
  import { ACMButton } from 'components/basic/ACM';

  const { ActionButton } = ACMButton;

  ReactDOM.render(
    <div>
      <ActionButton type="info" icon="search" />
      <ActionButton type="danger" icon={<ACMIconDelete />} />
      <ActionButton type="danger" size="large" icon={<ACMIconDelete />} />
      <ActionButton type="danger" shape="circle-outline" icon={<ACMIconDelete />} />
    </div>,
    mountNode,
  );
`;
const { ACMIconDelete, ACMIconEdit } = ACMIcon;
const { ActionButton } = ACMButton;
const GeneralButtonAction = () => {
  return (
    <MainTemplateCard code={code} title="Basic action button">
      <div className={styles.action__button__demo}>
        <ActionButton type="info" icon={<SearchOutlined />} />
        <ActionButton type="danger" icon={<ACMIconDelete />} />
        <ActionButton type="danger" size="large" icon={<ACMIconDelete />} />
        <ActionButton type="danger" shape="circle-outline" icon={<ACMIconDelete />} />
        <ActionButton type="warn" tooltip="Edit" shape="circle" icon={<ACMIconDelete />} />
        <ActionButton type="danger" tooltip="Delete" shape="circle" icon={<ACMIconEdit />} />
      </div>
    </MainTemplateCard>
  );
};

GeneralButtonAction.propTypes = {};

export default memo(GeneralButtonAction);
