import React, { memo } from 'react';
import { ACMButton, ACMIcon } from 'components/basic/ACM';
import MainTemplateCard from 'components/group/MainTemplateCard';
import styles from './Tooltip.module.less';

const code = `
  import { ACMButton } from 'components/basic/ACM';

  const { ActionButton } = ACMButton;

  ReactDOM.render(
    <div>
      <ActionButton type="info" tooltip="Search" shape="circle" icon="search" />
      <ActionButton type="gray" tooltip="Search" shape="circle" icon="search" />
      <ActionButton type="blue" tooltip="Search" shape="circle" icon="search" />
      <ActionButton type="warn" tooltip="Delete" shape="circle" icon="delete" />
      <ActionButton type="success" tooltip="Delete" icon="delete" />
      <ActionButton type="danger" tooltip="Delete" size="large" icon="delete" />
      <ActionButton type="danger" tooltip="Delete" shape="circle" icon="delete" />
    </div>,
    mountNode,
  );
`;
const { ACMIconDelete, ACMIconEdit } = ACMIcon;
const { ActionButton } = ACMButton;
const GeneralButtonTooltip = () => {
  return (
    <MainTemplateCard code={code} title="Action button with tooltip and color">
      <div className={styles.action__button__demo}>
        <ActionButton type="info" tooltip="Search" shape="circle" icon={<ACMIconDelete />} />
        <ActionButton type="gray" tooltip="Search" shape="circle" icon={<ACMIconDelete />} />
        <ActionButton type="blue" tooltip="Search" shape="circle" icon={<ACMIconDelete />} />
        <ActionButton type="warn" tooltip="Delete" shape="circle" icon={<ACMIconDelete />} />
        <ActionButton type="success" tooltip="Delete" icon={<ACMIconEdit />} />
        <ActionButton type="danger" tooltip="Delete" size="large" icon={<ACMIconEdit />} />
        <ActionButton type="danger" tooltip="Delete" shape="circle" icon={<ACMIconEdit />} />
      </div>
    </MainTemplateCard>
  );
};

GeneralButtonTooltip.propTypes = {};

export default memo(GeneralButtonTooltip);
