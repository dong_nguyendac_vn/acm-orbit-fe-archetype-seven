import React, { memo } from 'react';
import { ACMButton } from 'components/basic/ACM';
import { SearchOutlined } from '@ant-design/icons';
import MainTemplateCard from 'components/group/MainTemplateCard';
import styles from './Basic.module.less';

const code = `
  import { ACMButton } from 'components/basic/ACM';

  ReactDOM.render(
    <div>
      <ACMButton size="large" icon="search">Search</ACMButton>
      <ACMButton>Default</ACMButton>
      <ACMButton size="small" type="dashed">Dashed</ACMButton>
      <ACMButton type="danger">Danger</ACMButton>
      <ACMButton type="link">Link</ACMButton>
    </div>,
    mountNode,
  );
`;
const GeneralButtonBasic = () => {
  return (
    <MainTemplateCard code={code} title="Basic button">
      <div className={styles.basic__button__demo}>
        <ACMButton size="large" icon={<SearchOutlined />}>
          Search
        </ACMButton>
        <ACMButton>Default</ACMButton>
        <ACMButton type="dashed">Dashed</ACMButton>
        <ACMButton size="small" type="danger">
          Danger
        </ACMButton>
        <ACMButton type="link">Link</ACMButton>
      </div>
    </MainTemplateCard>
  );
};

GeneralButtonBasic.propTypes = {};

export default memo(GeneralButtonBasic);
