import React, { PureComponent } from 'react';
import { ACMRow, ACMCol, ACMPageContent } from 'components/basic/ACM';
import GeneralButtonBasic from './Components/Basic';
import GeneralButtonAction from './Components/Action';
import GeneralButtonTooltip from './Components/Tooltip';

class GeneralButtonScreen extends PureComponent {
  render() {
    return (
      <ACMPageContent>
        <div className="acm-checkbox-screen">
          <ACMRow gutter={[16, 16]}>
            <ACMCol span={8}>
              <GeneralButtonBasic />
            </ACMCol>
            <ACMCol span={8}>
              <GeneralButtonAction />
            </ACMCol>
            <ACMCol span={8}>
              <GeneralButtonTooltip />
            </ACMCol>
          </ACMRow>
        </div>
      </ACMPageContent>
    );
  }
}
GeneralButtonScreen.propTypes = {};
export default GeneralButtonScreen;
