import React, { PureComponent } from 'react';
import { MainScreenHeaderContainer } from 'containers/group';
import classNames from 'classnames';
import { ACMPageContent, ACMCard } from 'components/basic/ACM';
import { FormTemplateData } from 'components/group/Form/Template';
import styles from './Edit.screen.module.less';

class TemplateEditScreen extends PureComponent {
  onSubmit = e => {
    e.preventDefault();
  };

  render() {
    const clsString = classNames(styles.edit_template_screen);
    return (
      <div className={clsString}>
        <MainScreenHeaderContainer title="Edit Template" />
        <ACMPageContent>
          <ACMCard>
            <FormTemplateData />
          </ACMCard>
        </ACMPageContent>
      </div>
    );
  }
}

TemplateEditScreen.defaultProps = {};

TemplateEditScreen.propTypes = {};

export default TemplateEditScreen;
