import React, { PureComponent } from 'react';
import { MainScreenHeaderContainer } from 'containers/group';
import classNames from 'classnames';
import {
  ACMPageContent,
  ACMSearchCriteria,
  ACMCard,
  ACMTableContent,
  ACMModal,
} from 'components/basic/ACM';
import { FormTemplateSearch, FormTemplateData } from 'components/group/Form/Template';
import { TableTemplatePage } from 'components/group/Table/Template';
import PropTypes from 'prop-types';
import styles from './Search.screen.module.less';

const data = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park, New York No. 1 Lake Park',
    tags: ['nice', 'developer'],
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 2 Lake Park, London No. 2 Lake Park',
    tags: ['loser'],
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park, Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
  {
    key: '4',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park, Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
  {
    key: '5',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park, Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
  {
    key: '6',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park, Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
  {
    key: '7',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park, Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
  {
    key: '8',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park, Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
];

const { DataModal } = ACMModal;

class TemplateSearchScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  onSubmit = e => {
    e.preventDefault();
  };

  openDetail = () => {
    const { templateShowDetail } = this.props;
    templateShowDetail({ id: 1 });
  };

  openModal = () => {
    this.setState({ visible: true });
  };

  hideModal = () => {
    this.setState({ visible: false });
  };

  render() {
    const { visible } = this.state;
    const clsString = classNames(styles.search_template_screen);
    return (
      <div className={clsString}>
        <MainScreenHeaderContainer title="Search Template" />
        <ACMPageContent>
          <ACMSearchCriteria>
            <FormTemplateSearch onFinish={this.onSubmit} />
          </ACMSearchCriteria>
          <ACMTableContent>
            <ACMCard>
              <TableTemplatePage
                dataSource={data}
                onView={this.openModal}
                onEdit={this.openDetail}
              />
            </ACMCard>
          </ACMTableContent>
        </ACMPageContent>
        {visible && (
          <DataModal visible={visible} title="Edit Form" onCancel={this.hideModal}>
            <FormTemplateData onFinish={this.hideModal} onCancel={this.hideModal} />
          </DataModal>
        )}
      </div>
    );
  }
}

TemplateSearchScreen.defaultProps = {
  templateShowDetail: () => {},
};

TemplateSearchScreen.propTypes = {
  templateShowDetail: PropTypes.func,
};

export default TemplateSearchScreen;
