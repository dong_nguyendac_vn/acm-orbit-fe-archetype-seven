import React, { memo } from 'react';
import { ACMTooltip } from 'components/basic/ACM';
import MainTemplateCard from 'components/group/MainTemplateCard';
import styles from './Basic.module.less';

const code = `
  import { ACMTooltip } from "components/basic/ACM";

  ReactDOM.render(
     <div>
      ACMTooltip title="Example tooltip">Top</ACMTooltip>
      <ACMTooltip title="Example tooltip" placement="bottom">
        Bottom
      </ACMTooltip>
      <ACMTooltip title="Example tooltip" placement="left">
        Left
      </ACMTooltip>
      <ACMTooltip title="Example tooltip" placement="right">
        Right
      </ACMTooltip>
    </div>, 
  mountNode);
`;
const BasicTooltip = () => {
  return (
    <MainTemplateCard code={code} title="Basic tooltip">
      <div className={styles.acm__basic__tooltip__demo}>
        <ACMTooltip title="Example tooltip">Top</ACMTooltip>
        <ACMTooltip title="Example tooltip" placement="bottom">
          Bottom
        </ACMTooltip>
        <ACMTooltip title="Example tooltip" placement="left">
          Left
        </ACMTooltip>
        <ACMTooltip title="Example tooltip" placement="right">
          Right
        </ACMTooltip>
      </div>
    </MainTemplateCard>
  );
};

BasicTooltip.propTypes = {};

export default memo(BasicTooltip);
