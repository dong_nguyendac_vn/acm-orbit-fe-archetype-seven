import React, { PureComponent } from 'react';
import { ACMPageContent } from 'components/basic/ACM';
import BasicTooltip from './Components/Basic';

class TooltipScreen extends PureComponent {
  render() {
    return (
      <ACMPageContent>
        <div className="acm-tooltip-screen">
          <BasicTooltip />
        </div>
      </ACMPageContent>
    );
  }
}
TooltipScreen.propTypes = {};
export default TooltipScreen;
