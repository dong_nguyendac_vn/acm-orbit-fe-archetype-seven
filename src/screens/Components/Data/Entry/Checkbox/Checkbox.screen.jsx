import React, { PureComponent } from 'react';
import { ACMPageContent } from 'components/basic/ACM';
import BasicCheckbox from './Components/Basic';

class CheckboxScreen extends PureComponent {
  render() {
    return (
      <ACMPageContent>
        <div className="acm-checkbox-screen">
          <BasicCheckbox />
        </div>
      </ACMPageContent>
    );
  }
}
CheckboxScreen.propTypes = {};
export default CheckboxScreen;
