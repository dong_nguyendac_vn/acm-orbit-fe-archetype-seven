import React, { memo } from 'react';
import { ACMCheckbox } from 'components/basic/ACM';
import MainTemplateCard from 'components/group/MainTemplateCard';

const code = `
  import { ACMCheckbox } from "components/basic/ACM";

  function onChange(e) {
    console.log(\`checked = \${e.target.checked}\`);
  }

  ReactDOM.render(<Checkbox onChange={onChange}>Checkbox</Checkbox>, mountNode);
`;
const BasicCheckbox = () => {
  return (
    <MainTemplateCard code={code} title="Basic check box">
      <ACMCheckbox>Check box</ACMCheckbox>
    </MainTemplateCard>
  );
};

BasicCheckbox.propTypes = {};

export default memo(BasicCheckbox);
