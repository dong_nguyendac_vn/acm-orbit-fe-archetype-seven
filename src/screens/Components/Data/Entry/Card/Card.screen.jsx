import React, { PureComponent } from 'react';
import { ACMPageContent } from 'components/basic/ACM';
import BasicCard from './Components/Basic';

class CardScreen extends PureComponent {
  render() {
    return (
      <ACMPageContent>
        <div className="acm-card-screen">
          <BasicCard />
        </div>
      </ACMPageContent>
    );
  }
}
CardScreen.propTypes = {};
export default CardScreen;
