import React, { memo } from 'react';
import { ACMCard } from 'components/basic/ACM';
import MainTemplateCard from 'components/group/MainTemplateCard';
import styles from './Basic.module.less';

const code = `
  import { ACMCard } from 'components/basic/ACM';

  ReactDOM.render(
    <ACMCard>
      <p>Card content</p>
      <p>Card content</p>
      <p>Card content</p>
    </ACMCard>,
    mountNode,
  );
`;
const BasicCard = () => {
  return (
    <MainTemplateCard code={code} title="Basic card" className={styles.acm__card__basic__demo}>
      <ACMCard>
        <p>Card content</p>
        <p>Card content</p>
        <p>Card content</p>
      </ACMCard>
    </MainTemplateCard>
  );
};

BasicCard.propTypes = {};

export default memo(BasicCard);
