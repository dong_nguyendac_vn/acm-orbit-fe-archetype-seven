import TemplateSearchScreen from 'screens/Components/Template/Search';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TemplateAction from 'redux/actions/template.action';

const { templateShowDetail } = TemplateAction;

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    templateShowDetail: bindActionCreators(templateShowDetail, dispatch),
  };
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
  };
}

const TemplateSearchScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(TemplateSearchScreen);

export default TemplateSearchScreenContainer;
