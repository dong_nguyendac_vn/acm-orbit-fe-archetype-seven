import PartnerSearchScreen from 'screens/Partner/Search';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PartnerAction from 'redux/actions/partner.action';

const { partnerFetch } = PartnerAction;

function mapStateToProps({ partner }) {
  return {
    partner: {
      data: partner.get('data'),
      page: partner.get('page'),
    },
  };
}

function mapDispatchToProps(dispatch) {
  return {
    partnerFetch: bindActionCreators(partnerFetch, dispatch),
  };
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
  };
}

const PartnerSearchScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(PartnerSearchScreen);

export default PartnerSearchScreenContainer;
