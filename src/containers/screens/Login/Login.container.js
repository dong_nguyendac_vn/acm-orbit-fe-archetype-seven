import LoginScreen from 'screens/Login';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import UserAction from 'redux/actions/user.action';

const { userLogin } = UserAction;
function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    login: bindActionCreators(userLogin, dispatch),
  };
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
  };
}

const LoginContainer = connect(mapStateToProps, mapDispatchToProps, mergeProps)(LoginScreen);

export default LoginContainer;
