import { AppLoading } from 'components/group';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import UserAction from 'redux/actions/user.action';

const { userLoadInfo } = UserAction;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    userLoadInfo: bindActionCreators(userLoadInfo, dispatch),
  };
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
  };
}

const AppLoadingContainer = connect(mapStateToProps, mapDispatchToProps, mergeProps)(AppLoading);

export default AppLoadingContainer;
