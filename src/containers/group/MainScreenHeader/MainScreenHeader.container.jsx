import { MainScreenHeader } from 'components/group';
import { connect } from 'react-redux';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps() {
  return {};
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
  };
}

const MainScreenHeaderContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(MainScreenHeader);

export default MainScreenHeaderContainer;
