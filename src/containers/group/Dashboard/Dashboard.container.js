import { Dashboard } from 'components/group';
import { connect } from 'react-redux';
import AppMenu from 'configs/menus';
import AppRoutes from 'configs/routes';
import { bindActionCreators } from 'redux';

import AppAction from 'redux/actions/app.action';
import UserAction from 'redux/actions/user.action';

const { userLogout } = UserAction;
const { appOpenMenu, appSelectMenu, appCollapseMenu } = AppAction;

function mapStateToProps({ user, app }) {
  return {
    user: {
      profile: user.get('profile'),
    },
    app: {
      collapsedMenu: app.get('collapsedMenu'),
      currentMenuKey: app.get('currentMenuKey'),
      openMenuKeys: app.get('openMenuKeys').toArray(),
    },
    menus: AppMenu,
    dashboardRoutes: AppRoutes,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    appOpenMenu: bindActionCreators(appOpenMenu, dispatch),
    appSelectMenu: bindActionCreators(appSelectMenu, dispatch),
    logOut: bindActionCreators(userLogout, dispatch),
    collapseMenu: bindActionCreators(appCollapseMenu, dispatch),
  };
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
  };
}

const DashboardContainer = connect(mapStateToProps, mapDispatchToProps, mergeProps)(Dashboard);

export default DashboardContainer;
