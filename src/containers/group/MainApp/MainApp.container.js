import { MainApp } from 'components/group';
import { connect } from 'react-redux';

function mapStateToProps({ user }) {
  return {
    user: {
      profile: user.get('profile'),
    },
  };
}

function mapDispatchToProps() {
  return {};
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
  };
}

const MainAppContainer = connect(mapStateToProps, mapDispatchToProps, mergeProps)(MainApp);

export default MainAppContainer;
