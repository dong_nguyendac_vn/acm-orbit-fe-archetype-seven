import React from 'react';
import withBreadcrumbs from 'react-router-breadcrumbs-hoc';
import { HomeOutlined } from '@ant-design/icons';
import { ACMBreadcrumb } from 'components/basic/ACM';
import routes from 'configs/routes';
import { DASHBOARD_ROUTE_PREFIX } from 'configs/config';

const HomeBreadCrumb = () => <HomeOutlined />;

const routeCustom = routes
  .filter(r => r.breadCrumb)
  .map(fr => ({
    path: `${DASHBOARD_ROUTE_PREFIX}${fr.path}`,
    breadcrumb: fr.breadCrumb,
  }));
const CustomBreadCrumbRoutes = [
  { path: `${DASHBOARD_ROUTE_PREFIX}`, breadcrumb: HomeBreadCrumb },
  ...routeCustom,
];

export default withBreadcrumbs(CustomBreadCrumbRoutes, {
  excludePaths: ['/'],
  disableDefaults: true,
})(ACMBreadcrumb);
