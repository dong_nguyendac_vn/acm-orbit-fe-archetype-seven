import { KeycloakWrapper } from 'components/basic';
import { withCookies } from 'react-cookie';

export default withCookies(KeycloakWrapper);
