import PropTypes from 'prop-types';
import { checkPermissions } from 'utils/helpers';

const AccessControl = ({
  user,
  userPermissions,
  allowedPermissions,
  type,
  children,
  renderNoAccess,
  accessCheck,
  extraAccessData,
}) => {
  let permitted =
    accessCheck(extraAccessData, user) &&
    checkPermissions(userPermissions, allowedPermissions, type);
  // when an accessCheck function is provided, ensure that passes as well as the permissions
  if (!accessCheck) {
    // otherwise only check permissions
    permitted = checkPermissions(userPermissions, allowedPermissions, type);
  }

  if (permitted) {
    return children;
  }
  return renderNoAccess();
};

AccessControl.defaultProps = {
  allowedPermissions: [],
  userPermissions: [],
  type: 'some',
  renderNoAccess: () => null,
};

AccessControl.propTypes = {
  allowedPermissions: PropTypes.arrayOf(PropTypes.string),
  renderNoAccess: PropTypes.func,
  type: PropTypes.string,
  userPermissions: PropTypes.arrayOf(PropTypes.string),
};

export default AccessControl;
