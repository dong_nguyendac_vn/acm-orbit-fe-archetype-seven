import React, { PureComponent } from 'react';
import Keycloak from 'configs/keycloak';
import { Cookies } from 'react-cookie';
import PropTypes from 'prop-types';

class KeycloakWrapper extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false,
    };
  }

  componentDidMount() {
    const { cookies } = this.props;
    cookies.set('XSRF-TOKEN', 'ffb79533-dbe9-434f-9a46-488247ca306b');
    Keycloak.init({ onLoad: 'login-required', promiseType: 'native' }).then(authenticated => {
      this.setState({ authenticated });
      cookies.set('ACM-ACCESS-TOKEN', Keycloak.token);
    });
  }

  render() {
    const { authenticated } = this.state;
    const { children } = this.props;
    if (authenticated) {
      return <>{children}</>;
    }
    return <div>Initializing Keycloak...</div>;
  }
}

KeycloakWrapper.propTypes = {
  children: PropTypes.element.isRequired,
  cookies: PropTypes.instanceOf(Cookies).isRequired,
};

export default KeycloakWrapper;
