import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as Logo } from './logo.svg';

function ACMLogo({ className }) {
  return (
    <div className={className}>
      <Logo />
    </div>
  );
}

ACMLogo.defaultProps = {
  className: undefined,
};

ACMLogo.propTypes = {
  className: PropTypes.string,
};

export default memo(ACMLogo);
