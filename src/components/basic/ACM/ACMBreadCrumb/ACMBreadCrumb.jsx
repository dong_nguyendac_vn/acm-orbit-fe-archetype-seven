import React, { memo } from 'react';
import { Breadcrumb } from 'antd';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const { Item } = Breadcrumb;

const ACMBreadCrumb = ({ breadcrumbs, param }) => {
  return (
    <Breadcrumb>
      {breadcrumbs.map(breadcrumb => (
        <Item key={breadcrumb.key}>
          <Link to={breadcrumb.match && breadcrumb.match.url}>
            {breadcrumb.breadcrumb}{' '}
            {breadcrumb.match && breadcrumb.match.params && breadcrumb.match.params[param]}
          </Link>
        </Item>
      ))}
    </Breadcrumb>
  );
};

ACMBreadCrumb.defaultProps = {
  breadcrumbs: [],
  param: 'id',
};

ACMBreadCrumb.propTypes = {
  breadcrumbs: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string,
      match: PropTypes.shape({
        path: PropTypes.string,
        url: PropTypes.string,
        isExact: PropTypes.bool,
        params: PropTypes.object,
      }),
      breadcrumb: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    }),
  ),
  param: PropTypes.string,
};

export default memo(ACMBreadCrumb);
