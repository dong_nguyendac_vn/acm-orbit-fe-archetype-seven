import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './ACMPageContent.module.less';

const ACMPageContent = ({ children, className, style }) => {
  const clsString = classNames(styles.acm__page__content, className);
  return (
    <div className={clsString} style={style}>
      {children}
    </div>
  );
};

ACMPageContent.defaultProps = {
  className: undefined,
  children: undefined,
  style: undefined,
};

ACMPageContent.propTypes = {
  className: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

export default memo(ACMPageContent);
