import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Input } from 'antd';

const { TextArea } = Input;

const ACMTextArea = ({ autoSize, value, onPressEnter, allowClear, rows, cols, defaultValue }) => {
  return (
    <TextArea
      autoSize={autoSize}
      value={value}
      onPressEnter={onPressEnter}
      allowClear={allowClear}
      rows={rows}
      cols={cols}
      defaultValue={defaultValue}
    />
  );
};

ACMTextArea.defaultProps = {
  autoSize: true,
  value: '',
  onPressEnter: () => {},
  allowClear: true,
  rows: 4,
  cols: 4,
  defaultValue: '',
};

ACMTextArea.propTypes = {
  onPressEnter: PropTypes.func,
  allowClear: PropTypes.bool,
  defaultValue: PropTypes.string,
  rows: PropTypes.number,
  cols: PropTypes.number,
  value: PropTypes.string,
  autoSize: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      minRows: PropTypes.number,
      maxRows: PropTypes.number,
    }),
  ]),
};

export default memo(ACMTextArea);
