import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Input } from 'antd';

const ACMInput = ({
  autoFocus,
  size,
  suffix,
  type,
  id,
  disabled,
  required,
  value,
  readOnly,
  onChange,
  allowClear,
}) => {
  return (
    <Input
      id={id}
      autoFocus={autoFocus}
      size={size}
      suffix={suffix}
      type={type}
      disabled={disabled}
      readOnly={readOnly}
      allowClear={allowClear}
      value={value}
      onChange={onChange}
      required={required}
    />
  );
};

ACMInput.defaultProps = {
  size: 'default',
  suffix: '',
  type: 'text',
  autoFocus: false,
  id: undefined,
  allowClear: true,
  required: false,
  disabled: false,
  value: undefined,
  readOnly: false,
  onChange: undefined,
};

ACMInput.propTypes = {
  autoFocus: PropTypes.bool,
  required: PropTypes.bool,
  readOnly: PropTypes.bool,
  size: PropTypes.string,
  suffix: PropTypes.string,
  allowClear: PropTypes.bool,
  type: PropTypes.string,
  id: PropTypes.string,
  disabled: PropTypes.bool,
  value: PropTypes.string,
  onChange: PropTypes.func,
};

export default memo(ACMInput);
