import React, { memo, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Input } from 'antd';

const { Search } = Input;

const ACMInputSearch = forwardRef(
  (
    {
      enterButton,
      onSearch,
      loading,
      size,
      suffix,
      type,
      id,
      disabled,
      defaultValue,
      allowClear,
      required,
      value,
      onChange,
      readOnly,
      autoFocus,
      onClick,
    },
    ref,
  ) => {
    return (
      <Search
        id={id}
        autoFocus={autoFocus}
        allowClear={allowClear}
        size={size}
        suffix={suffix}
        loading={loading}
        type={type}
        disabled={disabled}
        readOnly={readOnly}
        ref={ref}
        value={value}
        onClick={onClick}
        onChange={onChange}
        required={required}
        defaultValue={defaultValue}
        enterButton={enterButton}
        onSearch={onSearch}
      />
    );
  },
);

ACMInputSearch.defaultProps = {
  defaultValue: '',
  autoFocus: false,
  size: 'default',
  suffix: '',
  type: 'text',
  id: undefined,
  allowClear: true,
  required: false,
  disabled: false,
  value: undefined,
  readOnly: false,
  onChange: () => {},
  onClick: () => {},
  loading: false,
  onSearch: undefined,
  enterButton: false,
};

ACMInputSearch.propTypes = {
  required: PropTypes.bool,
  autoFocus: PropTypes.bool,
  readOnly: PropTypes.bool,
  size: PropTypes.string,
  suffix: PropTypes.string,
  type: PropTypes.string,
  id: PropTypes.string,
  disabled: PropTypes.bool,
  defaultValue: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onSearch: PropTypes.func,
  enterButton: PropTypes.bool,
  loading: PropTypes.bool,
  onClick: PropTypes.func,
  allowClear: PropTypes.bool,
};

export default memo(ACMInputSearch);
