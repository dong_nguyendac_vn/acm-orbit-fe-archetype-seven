import ACMInput from './ACMInput';
import ACMTextArea from './ACMTextArea';
import ACMInputSearch from './ACMInputSearch';

ACMInput.TextArea = ACMTextArea;
ACMInput.Search = ACMInputSearch;
export default ACMInput;
