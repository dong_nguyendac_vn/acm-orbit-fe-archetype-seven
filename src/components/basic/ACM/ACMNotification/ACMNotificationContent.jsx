import React from 'react';
import ACMTypography from 'components/basic/ACM/ACMTypography';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './ACMNotificationContent.module.less';

const { ACMParagraph } = ACMTypography;

const ACMNotificationContent = ({ value, className }) => {
  const clssString = classNames(styles.acm__notification__content, className);
  return (
    <ACMParagraph className={clssString} ellipsis={{ rows: 2 }}>
      {value}
    </ACMParagraph>
  );
};
ACMNotificationContent.defaultProps = {
  value: '',
  className: undefined,
};
ACMNotificationContent.propTypes = {
  value: PropTypes.string,
  className: PropTypes.string,
};

export default ACMNotificationContent;
