import React from 'react';
import { notification } from 'antd';
import ACMNotificationContent from './ACMNotificationContent';

const open = ({ type, title, description, placement, key }) => {
  notification[type || 'info']({
    message: title,
    key,
    placement,
    description: <ACMNotificationContent value={description} />,
  });
};

const close = key => {
  notification.close(key);
};

const destroy = () => {
  notification.destroy();
};

const ACMNotification = {
  open,
  close,
  destroy,
};
export default ACMNotification;
