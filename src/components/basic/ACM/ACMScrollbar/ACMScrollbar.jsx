import React, { memo } from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from '@opuscapita/react-perfect-scrollbar';

const ACMScrollbar = ({ children, className }) => {
  return <PerfectScrollbar className={className}>{children}</PerfectScrollbar>;
};

ACMScrollbar.defaultProps = {
  className: undefined,
};

ACMScrollbar.propTypes = {
  className: PropTypes.string,
  children: PropTypes.element.isRequired,
};

export default memo(ACMScrollbar);
