import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';

const { Text } = Typography;

const ACMText = ({ ellipsis, underline, strong, deleted, children, className }) => {
  return (
    <Text
      ellipsis={ellipsis}
      strong={strong}
      underline={underline}
      delete={deleted}
      className={className}
    >
      {children}
    </Text>
  );
};

ACMText.defaultProps = {
  ellipsis: true,
  underline: false,
  deleted: false,
  children: '',
  className: '',
  strong: false,
};

ACMText.propTypes = {
  ellipsis: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      rows: PropTypes.number,
      expandable: PropTypes.bool,
      onExpand: PropTypes.func,
    }),
  ]),
  strong: PropTypes.bool,
  deleted: PropTypes.bool,
  underline: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.element, PropTypes.func]),
};

export default memo(ACMText);
