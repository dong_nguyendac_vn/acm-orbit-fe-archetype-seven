import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';

const { Title } = Typography;

const ACMTitle = ({ level, ellipsis, underline, deleted, children, className }) => {
  return (
    <Title
      level={level}
      ellipsis={ellipsis}
      underline={underline}
      delete={deleted}
      className={className}
    >
      {children}
    </Title>
  );
};

ACMTitle.defaultProps = {
  level: 1,
  ellipsis: true,
  underline: false,
  deleted: false,
  children: '',
  className: '',
};

ACMTitle.propTypes = {
  ellipsis: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      rows: PropTypes.number,
      expandable: PropTypes.bool,
      onExpand: PropTypes.func,
    }),
  ]),
  deleted: PropTypes.bool,
  level: PropTypes.oneOf([1, 2, 3, 4]),
  underline: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.element, PropTypes.func]),
};

export default memo(ACMTitle);
