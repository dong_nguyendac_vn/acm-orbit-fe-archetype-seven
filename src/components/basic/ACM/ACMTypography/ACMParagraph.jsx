import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';

const { Paragraph } = Typography;

const ACMParagraph = ({ strong, deleted, underline, disabled, children, className, ellipsis }) => {
  return (
    <Paragraph
      strong={strong}
      disabled={disabled}
      ellipsis={ellipsis}
      underline={underline}
      delete={deleted}
      className={className}
    >
      {children}
    </Paragraph>
  );
};

ACMParagraph.defaultProps = {
  strong: false,
  ellipsis: false,
  underline: false,
  deleted: false,
  children: '',
  className: '',
  disabled: false,
};

ACMParagraph.propTypes = {
  ellipsis: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      rows: PropTypes.number,
      expandable: PropTypes.bool,
      onExpand: PropTypes.func,
    }),
  ]),
  disabled: PropTypes.bool,
  strong: PropTypes.bool,
  deleted: PropTypes.bool,
  underline: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.element, PropTypes.func]),
};

export default memo(ACMParagraph);
