import ACMTitle from './ACMTitle';
import ACMParagraph from './ACMParagraph';
import ACMText from './ACMText';

export default { ACMParagraph, ACMTitle, ACMText };
