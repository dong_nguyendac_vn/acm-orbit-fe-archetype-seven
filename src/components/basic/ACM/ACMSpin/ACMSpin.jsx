import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Spin } from 'antd';

const ACMSpin = ({ wrapperClassName, delay, indicator, size, spinning, tip }) => {
  return React.createElement(Spin, { wrapperClassName, indicator, delay, size, spinning, tip });
};
ACMSpin.defaultProps = {
  wrapperClassName: undefined,
  delay: undefined,
  size: 'default',
  spinning: undefined,
  tip: undefined,
  indicator: undefined,
};
ACMSpin.propTypes = {
  wrapperClassName: PropTypes.string,
  delay: PropTypes.number,
  size: PropTypes.oneOf(['small', 'default', 'large']),
  spinning: PropTypes.bool,
  tip: PropTypes.string,
  indicator: PropTypes.node,
};

export default memo(ACMSpin);
