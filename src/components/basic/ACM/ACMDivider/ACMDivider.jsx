import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Divider } from 'antd';

const ACMDivider = ({ dashed, orientation, style, className, type }) => {
  return (
    <Divider
      dashed={dashed}
      type={type}
      style={style}
      className={className}
      orientation={orientation}
    />
  );
};

ACMDivider.defaultProps = {
  orientation: 'center',
  type: 'horizontal',
  style: {},
  className: '',
  dashed: false,
};

ACMDivider.propTypes = {
  orientation: PropTypes.string,
  type: PropTypes.string,
  dashed: PropTypes.bool,
  style: PropTypes.objectOf(PropTypes.string),
  className: PropTypes.oneOfType([PropTypes.string]),
};

export default memo(ACMDivider);
