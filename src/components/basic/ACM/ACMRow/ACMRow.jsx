import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Row } from 'antd';

const ACMRow = ({ align, gutter, justify, type, children, className, style }) => {
  return (
    <Row
      align={align}
      gutter={gutter}
      justify={justify}
      type={type}
      className={className}
      style={style}
    >
      {children}
    </Row>
  );
};

ACMRow.defaultProps = {
  align: 'top',
  gutter: 0,
  justify: 'start',
  type: undefined,
  children: undefined,
  className: undefined,
  style: undefined,
};

ACMRow.propTypes = {
  align: PropTypes.oneOf(['top', 'middle', 'bottom']),
  gutter: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.shape({ xs: PropTypes.number, sm: PropTypes.number, md: PropTypes.number }),
    PropTypes.arrayOf(PropTypes.number),
  ]),
  justify: PropTypes.oneOf(['start', 'end', 'center', 'space-around', 'space-between']),
  type: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
  className: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

export default memo(ACMRow);
