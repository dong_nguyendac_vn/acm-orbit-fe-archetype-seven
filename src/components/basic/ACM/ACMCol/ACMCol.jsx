import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Col } from 'antd';

const ACMCol = ({ offset, order, span, children, className, xs, sm, md, lg, style }) => {
  return (
    <Col
      order={order}
      span={span}
      offset={offset}
      className={className}
      xs={xs}
      md={md}
      lg={lg}
      sm={sm}
      style={style}
    >
      {children}
    </Col>
  );
};

ACMCol.defaultProps = {
  offset: 0,
  order: 0,
  span: undefined,
  xs: undefined,
  sm: undefined,
  md: undefined,
  lg: undefined,
  children: undefined,
  className: undefined,
  style: undefined,
};

ACMCol.propTypes = {
  offset: PropTypes.number,
  order: PropTypes.number,
  span: PropTypes.number,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  xs: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.shape({
      span: PropTypes.number,
      order: PropTypes.number,
      pull: PropTypes.number,
      push: PropTypes.number,
      offset: PropTypes.number,
    }),
  ]),
  sm: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.shape({
      span: PropTypes.number,
      order: PropTypes.number,
      pull: PropTypes.number,
      push: PropTypes.number,
      offset: PropTypes.number,
    }),
  ]),
  md: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.shape({
      span: PropTypes.number,
      order: PropTypes.number,
      pull: PropTypes.number,
      push: PropTypes.number,
      offset: PropTypes.number,
    }),
  ]),
  lg: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.shape({
      span: PropTypes.number,
      order: PropTypes.number,
      pull: PropTypes.number,
      push: PropTypes.number,
      offset: PropTypes.number,
    }),
  ]),
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
};

export default memo(ACMCol);
