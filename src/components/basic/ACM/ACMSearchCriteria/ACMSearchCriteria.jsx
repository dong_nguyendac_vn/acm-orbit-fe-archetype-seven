import React, { memo } from 'react';
import ACMCard from 'components/basic/ACM/ACMCard/ACMCard';
import ACMTypography from 'components/basic/ACM/ACMTypography';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './ACMSearchCriteria.module.less';

const { ACMText } = ACMTypography;
const ACMSearchCriteria = ({ children, className, title }) => {
  const clsString = classNames(styles.acm__search__criteria, className);
  return (
    <ACMCard className={clsString}>
      <div className={styles.acm__search__title}>
        <ACMText ellipsis strong>
          {title}
        </ACMText>
      </div>
      <div className={styles.acm__search__content}>{children}</div>
    </ACMCard>
  );
};

ACMSearchCriteria.defaultProps = {
  className: undefined,
  title: 'Search Criteria',
  children: undefined,
};

ACMSearchCriteria.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

export default memo(ACMSearchCriteria);
