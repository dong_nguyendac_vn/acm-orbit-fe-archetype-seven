import React from 'react';
import { ReactComponent as Icon } from './SVG/ic-edit.svg';
import ACMIcon from './ACMIcon';

const ACMIconEdit = props => {
  return React.createElement(ACMIcon, { ...props, component: Icon });
};

export default ACMIconEdit;
