import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Icon from '@ant-design/icons';

const ACMIcon = ({ id, spin, style, className, onClick, component, theme }) => {
  return (
    <Icon
      id={id}
      spin={spin}
      style={style}
      theme={theme}
      component={component}
      className={className}
      onClick={onClick}
    />
  );
};

ACMIcon.defaultProps = {
  spin: false,
  style: {},
  id: undefined,
  className: '',
  component: undefined,
  theme: 'outlined',
  onClick: () => {},
};

ACMIcon.propTypes = {
  id: PropTypes.string,
  spin: PropTypes.bool,
  theme: PropTypes.string,
  component: PropTypes.elementType,
  onClick: PropTypes.func,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  className: PropTypes.oneOfType([PropTypes.string]),
};

export default memo(ACMIcon);
