import ACMIconCodeCollapsed from './ACMIconCodeCollapsed';
import ACMIconCodeExpanded from './ACMIconCodeExpanded';
import ACMIconDelete from './ACMIconDelete';
import ACMIconEdit from './ACMIconEdit';
import ACMIcon from './ACMIcon';

ACMIcon.ACMIconCodeCollapsed = ACMIconCodeCollapsed;
ACMIcon.ACMIconCodeExpanded = ACMIconCodeExpanded;
ACMIcon.ACMIconEdit = ACMIconEdit;
ACMIcon.ACMIconDelete = ACMIconDelete;

export default ACMIcon;
