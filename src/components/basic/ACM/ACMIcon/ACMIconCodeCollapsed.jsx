import React from 'react';
import { ReactComponent as Icon } from './SVG/ic-code-collapse.svg';
import ACMIcon from './ACMIcon';

const ACMIconCodeCollapsed = React.forwardRef((props, ref) => {
  return React.createElement(ACMIcon, { ...props, ref, component: Icon });
});

export default ACMIconCodeCollapsed;
