import React from 'react';
import { ReactComponent as Icon } from './SVG/ic-delete.svg';
import ACMIcon from './ACMIcon';

const ACMIconDelete = props => {
  return React.createElement(ACMIcon, { ...props, component: Icon });
};

export default ACMIconDelete;
