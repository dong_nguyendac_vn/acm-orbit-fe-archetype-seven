import React from 'react';
import { ReactComponent as Icon } from './SVG/ic-code-expanded.svg';
import ACMIcon from './ACMIcon';

const ACMIconCodeExpanded = props => {
  return React.createElement(ACMIcon, { ...props, component: Icon });
};

export default ACMIconCodeExpanded;
