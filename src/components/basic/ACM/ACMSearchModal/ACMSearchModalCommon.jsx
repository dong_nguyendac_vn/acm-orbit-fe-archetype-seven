import React, { PureComponent, cloneElement } from 'react';
import PropTypes from 'prop-types';
import ACMModal from 'components/basic/ACM/ACMModal';
import ACMInput from 'components/basic/ACM/ACMInput';
import classNames from 'classnames';
import styles from './ACMSearchModalCommon.module.less';

const { Search } = ACMInput;

class ACMSearchModalCommon extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  onClick = event => {
    const { ...rest } = this.props;
    if (!event && rest.value) {
      const { onChange } = this.props;
      onChange(null);
      return;
    }

    this.setState({ visible: true });
  };

  onCancel = () => {
    this.setState({ visible: false });
  };

  onSelect = record => {
    const { onChange, fields } = this.props;
    const { code, name } = fields;
    this.setState({ visible: false });
    onChange({ [code]: record[code], [name]: record[name] });
  };

  render() {
    const { title, className, children, fields, ...rest } = this.props;
    const { visible } = this.state;
    const { value } = rest;
    const clsString = classNames(styles.acm__search__modal__common, className);
    const cloneComponent = cloneElement(children, {
      popup: {
        onPopupCallBack: this.onSelect,
      },
    });
    return (
      <span className={clsString}>
        <Search
          value={(value && value[fields.name]) || ''}
          enterButton
          allowClear
          readOnly
          onSearch={this.onClick}
          onClick={this.onClick}
        />
        {cloneComponent && (
          <ACMModal
            className={styles.acm__search__modal__common__popup}
            visible={visible}
            title={title}
            footer={false}
            onCancel={this.onCancel}
          >
            {cloneComponent}
          </ACMModal>
        )}
      </span>
    );
  }
}
ACMSearchModalCommon.defaultProps = {
  title: 'Common',
  children: undefined,
  className: undefined,
  onChange: () => {},
  fields: {
    code: 'code',
    name: 'name',
  },
};

ACMSearchModalCommon.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.node, PropTypes.func]),
  title: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func,
  fields: PropTypes.shape({
    code: PropTypes.string,
    name: PropTypes.string,
  }),
};

export default ACMSearchModalCommon;
