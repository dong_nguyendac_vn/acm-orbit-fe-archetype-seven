import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Form } from 'antd';

const ACMForm = ({
  initialValues,
  children,
  labelAlign,
  className,
  onFinish,
  autoComplete,
  layout,
}) => {
  return (
    <Form
      layout={layout}
      initialValues={initialValues}
      labelAlign={labelAlign}
      className={className}
      onFinish={onFinish}
      autoComplete={autoComplete}
    >
      {children}
    </Form>
  );
};

ACMForm.defaultProps = {
  initialValues: {},
  children: undefined,
  className: undefined,
  layout: 'horizontal',
  autoComplete: 'off',
  labelAlign: 'right',
  onFinish: undefined,
};

ACMForm.propTypes = {
  initialValues: PropTypes.oneOfType([PropTypes.object]),
  layout: PropTypes.oneOf(['horizontal', 'vertical', 'inline']),
  labelAlign: PropTypes.oneOf(['left', 'right']),
  autoComplete: PropTypes.string,
  className: PropTypes.string,
  onFinish: PropTypes.func,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
};

export default memo(ACMForm);
