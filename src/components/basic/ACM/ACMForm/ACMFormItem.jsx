import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Form } from 'antd';
import styles from './ACMFormItem.module.less';

const { Item } = Form;
const ACMFormItem = ({
  htmlFor,
  label,
  className,
  labelAlign,
  required,
  children,
  labelCol,
  wrapperCol,
  name,
  hasFeedback,
  rules,
}) => {
  const clsString = classNames(styles.acm__form__item, className);

  return React.createElement(
    Item,
    {
      labelCol,
      wrapperCol,
      htmlFor,
      labelAlign,
      label,
      hasFeedback,
      name,
      rules,
      className: clsString,
      ...(required ? { required } : {}),
    },
    children,
  );
};

ACMFormItem.defaultProps = {
  children: undefined,
  className: undefined,
  name: undefined,
  rules: undefined,
  labelAlign: 'left',
  required: undefined,
  label: undefined,
  htmlFor: undefined,
  wrapperCol: undefined,
  labelCol: undefined,
  hasFeedback: undefined,
};

ACMFormItem.propTypes = {
  hasFeedback: PropTypes.bool,
  name: PropTypes.string,
  rules: PropTypes.arrayOf(
    PropTypes.shape({
      enum: PropTypes.arrayOf(PropTypes.any),
      len: PropTypes.number,
      max: PropTypes.number,
      message: PropTypes.string,
      min: PropTypes.number,
      pattern: PropTypes.instanceOf(RegExp),
      transform: PropTypes.func,
      type: PropTypes.string,
      validator: PropTypes.func,
      whitespace: PropTypes.bool,
      validateTrigger: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
    }),
  ),
  className: PropTypes.string,
  labelCol: PropTypes.shape({
    span: PropTypes.number,
    order: PropTypes.number,
    pull: PropTypes.number,
    push: PropTypes.number,
    xs: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.shape({
        span: PropTypes.number,
        order: PropTypes.number,
        pull: PropTypes.number,
        push: PropTypes.number,
        offset: PropTypes.number,
      }),
    ]),
    sm: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.shape({
        span: PropTypes.number,
        order: PropTypes.number,
        pull: PropTypes.number,
        push: PropTypes.number,
        offset: PropTypes.number,
      }),
    ]),
    md: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.shape({
        span: PropTypes.number,
        order: PropTypes.number,
        pull: PropTypes.number,
        push: PropTypes.number,
        offset: PropTypes.number,
      }),
    ]),
    lg: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.shape({
        span: PropTypes.number,
        order: PropTypes.number,
        pull: PropTypes.number,
        push: PropTypes.number,
        offset: PropTypes.number,
      }),
    ]),
  }),
  wrapperCol: PropTypes.shape({
    span: PropTypes.number,
    order: PropTypes.number,
    pull: PropTypes.number,
    push: PropTypes.number,
    xs: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.shape({
        span: PropTypes.number,
        order: PropTypes.number,
        pull: PropTypes.number,
        push: PropTypes.number,
        offset: PropTypes.number,
      }),
    ]),
    sm: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.shape({
        span: PropTypes.number,
        order: PropTypes.number,
        pull: PropTypes.number,
        push: PropTypes.number,
        offset: PropTypes.number,
      }),
    ]),
    md: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.shape({
        span: PropTypes.number,
        order: PropTypes.number,
        pull: PropTypes.number,
        push: PropTypes.number,
        offset: PropTypes.number,
      }),
    ]),
    lg: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.shape({
        span: PropTypes.number,
        order: PropTypes.number,
        pull: PropTypes.number,
        push: PropTypes.number,
        offset: PropTypes.number,
      }),
    ]),
  }),
  htmlFor: PropTypes.string,
  label: PropTypes.string,
  required: PropTypes.bool,
  labelAlign: PropTypes.oneOf(['left', 'right']),
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
};

export default memo(ACMFormItem);
