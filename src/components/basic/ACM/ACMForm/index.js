import ACMForm from './ACMForm';
import ACMFormItem from './ACMFormItem';
import ACMHorizontalFormItem from './ACMHorizontalFormItem';

ACMForm.Item = ACMFormItem;
ACMForm.HorizontalItem = ACMHorizontalFormItem;
export default ACMForm;
