import React, { memo } from 'react';
import PropTypes from 'prop-types';
import ACMFormItem from './ACMFormItem';

const itemHorizontalLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 24 },
    md: { span: 24 },
    lg: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 24 },
    md: { span: 24 },
    lg: { span: 16 },
  },
};

const ACMHorizontalFormItem = ({
  htmlFor,
  label,
  className,
  labelAlign,
  children,
  required,
  hasFeedback,
  name,
  rules,
}) => {
  const { labelCol, wrapperCol } = itemHorizontalLayout;
  return (
    <ACMFormItem
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      htmlFor={htmlFor}
      labelAlign={labelAlign}
      hasFeedback={hasFeedback}
      label={label}
      name={name}
      rules={rules}
      required={required}
      className={className}
    >
      {children}
    </ACMFormItem>
  );
};

ACMHorizontalFormItem.defaultProps = {
  children: undefined,
  className: undefined,
  labelAlign: 'left',
  label: undefined,
  htmlFor: undefined,
  required: undefined,
  name: undefined,
  rules: undefined,
  hasFeedback: false,
};

ACMHorizontalFormItem.propTypes = {
  required: PropTypes.bool,
  name: PropTypes.string,
  hasFeedback: PropTypes.bool,
  rules: PropTypes.arrayOf(
    PropTypes.shape({
      enum: PropTypes.arrayOf(PropTypes.any),
      len: PropTypes.number,
      max: PropTypes.number,
      message: PropTypes.string,
      min: PropTypes.number,
      pattern: PropTypes.instanceOf(RegExp),
      transform: PropTypes.func,
      type: PropTypes.string,
      validator: PropTypes.func,
      whitespace: PropTypes.bool,
      validateTrigger: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
    }),
  ),
  className: PropTypes.string,
  htmlFor: PropTypes.string,
  label: PropTypes.string,
  labelAlign: PropTypes.oneOf(['left', 'right']),
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
};

export default memo(ACMHorizontalFormItem);
