import React from 'react';
import PropTypes from 'prop-types';

const SvgIcCardManagement = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <g fill="none" fillRule="evenodd">
      <path d="M0 24h24V0H0z" />
      <g fill={color || 'currentColor'}>
        <path d="M4 18h16v-7H4v7zM4 8h16V6H4v2zm16-5H4a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h16a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3z" />
        <path d="M7 16.5h2a1.5 1.5 0 1 0 0-3H7a1.5 1.5 0 1 0 0 3" />
      </g>
    </g>
  </svg>
);

SvgIcCardManagement.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcCardManagement.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcCardManagement;
