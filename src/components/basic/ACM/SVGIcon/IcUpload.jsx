import React from 'react';
import PropTypes from 'prop-types';

const SvgIcUpload = ({ color, height, width, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <defs>
      <path
        id="a"
        d="M16.125 5.033A6.242 6.242 0 0 0 10 0a6.246 6.246 0 0 0-5.542 3.367A4.995 4.995 0 0 0 0 8.333c0 2.759 2.242 5 5 5h10.833c2.3 0 4.167-1.866 4.167-4.166 0-2.2-1.708-3.984-3.875-4.134zM11.667 7.5v3.333H8.333V7.5h-2.5L10 3.333 14.167 7.5h-2.5z"
      />
    </defs>
    <use
      fill={color || 'currentColor'}
      fillRule="evenodd"
      transform="translate(2 5)"
      xlinkHref="#a"
    />
  </svg>
);

SvgIcUpload.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcUpload.propTypes = {
  color: PropTypes.string,
  height: PropTypes.number,
  width: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcUpload;
