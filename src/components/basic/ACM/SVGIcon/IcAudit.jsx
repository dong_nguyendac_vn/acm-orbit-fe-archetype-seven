import React from 'react';
import PropTypes from 'prop-types';

const SvgIcAudit = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <path
      d="M17.157 14.325v.01c-.33.6-.73 1.14-1.21 1.62-.48.48-1.02.88-1.61 1.21-.99.54-2.13.84-3.34.84-3.86 0-7-3.14-7-7 0-3.87 3.14-7 7-7 3.87 0 7 3.13 7 7 0 1.2-.3 2.33-.84 3.32m5.26 5.26l-3.08-3.08a9.902 9.902 0 0 0 1.66-5.5c0-5.52-4.48-10-10-10-5.51 0-10 4.48-10 10 0 5.51 4.49 10 10 10 2.04 0 3.94-.61 5.52-1.66l3.07 3.07a1.993 1.993 0 0 0 2.83 0c.78-.78.78-2.05 0-2.83M10.136 13.75a1 1 0 0 1-.696-.281l-1.886-1.826a1 1 0 0 1 1.392-1.438l1.19 1.153 2.918-2.827a1 1 0 0 1 1.392 1.438l-3.614 3.5a1 1 0 0 1-.696.281"
      fill={color || 'currentColor'}
      fillRule="evenodd"
    />
  </svg>
);

SvgIcAudit.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcAudit.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcAudit;
