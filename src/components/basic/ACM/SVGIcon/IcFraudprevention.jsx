import React from 'react';
import PropTypes from 'prop-types';

const SvgIcFraudprevention = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <path
      d="M5 14.24a13.467 13.467 0 0 0 7 5.678 13.459 13.459 0 0 0 6.131-4.425c.308-.394.599-.814.869-1.252V6.76a26.671 26.671 0 0 1-7-2.56 26.671 26.671 0 0 1-7 2.56v7.48zM12 23c-.151 0-.303-.024-.449-.07a16.441 16.441 0 0 1-8.045-5.587A16.744 16.744 0 0 1 2.2 15.402a1.503 1.503 0 0 1-.2-.75v-9.13a1.5 1.5 0 0 1 1.253-1.48A23.77 23.77 0 0 0 11.226 1.2a1.623 1.623 0 0 1 1.548 0 23.77 23.77 0 0 0 7.973 2.843A1.5 1.5 0 0 1 22 5.522v9.13c0 .264-.069.522-.2.75a16.624 16.624 0 0 1-1.307 1.94 16.434 16.434 0 0 1-8.044 5.588c-.146.046-.298.07-.449.07zm-1.1-7.7a.996.996 0 0 1-.707-.293l-2.4-2.4a.999.999 0 1 1 1.414-1.414l1.693 1.693 3.893-3.894a1 1 0 0 1 1.414 1.414l-4.6 4.601a.996.996 0 0 1-.707.293"
      fill={color || 'currentColor'}
      fillRule="evenodd"
    />
  </svg>
);

SvgIcFraudprevention.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcFraudprevention.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcFraudprevention;
