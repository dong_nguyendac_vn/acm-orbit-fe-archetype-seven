import React from 'react';
import PropTypes from 'prop-types';

const SvgIcAgents = ({ color, width, height, viewBox }) => {
  return (
    <svg width={width} height={height} viewBox={viewBox}>
      <g fill="none" fillRule="evenodd">
        <path d="M0 24h24V0H0z" />
        <g fill={color || 'currentColor'}>
          <path d="M4 18h16V6H4v12zM20 3H4a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h16a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3z" />
          <path d="M7.5 16.5h5a1.495 1.495 0 1 0 .105-2.989A3.97 3.97 0 0 1 10 14.5c-1 0-1.903-.382-2.605-.989A1.494 1.494 0 0 0 6 15a1.5 1.5 0 0 0 1.5 1.5M10 13a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5" />
        </g>
      </g>
    </svg>
  );
};

SvgIcAgents.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcAgents.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcAgents;
