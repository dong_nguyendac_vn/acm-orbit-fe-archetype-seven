import React from 'react';
import PropTypes from 'prop-types';

const SvgIcSourceOfFund = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <g fill="none" fillRule="evenodd">
      <path d="M0 24h24V0H0z" />
      <path
        d="M20 9a2.998 2.998 0 0 0-2.734 1.779c-.051-.006-.096-.029-.148-.029h-1.387L11.59 3.387a1.251 1.251 0 0 0-1.09-.637H6.719A2.998 2.998 0 0 0 4 1C2.346 1 1 2.346 1 4s1.346 3 3 3a3 3 0 0 0 2.719-1.75h3.05l3.093 5.5H6.719A2.998 2.998 0 0 0 4 9c-1.654 0-3 1.346-3 3s1.346 3 3 3a3 3 0 0 0 2.719-1.75h6.143l-3.093 5.5h-3.05A2.998 2.998 0 0 0 4 17c-1.654 0-3 1.346-3 3s1.346 3 3 3a3 3 0 0 0 2.719-1.75H10.5c.451 0 .868-.243 1.09-.637l4.141-7.363h1.387c.052 0 .097-.023.148-.029A2.998 2.998 0 0 0 20 15c1.654 0 3-1.346 3-3s-1.346-3-3-3"
        fill={color || 'currentColor'}
      />
    </g>
  </svg>
);

SvgIcSourceOfFund.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcSourceOfFund.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcSourceOfFund;
