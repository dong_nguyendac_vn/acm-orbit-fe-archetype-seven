import React from 'react';
import PropTypes from 'prop-types';

const SvgIcSetting00 = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <defs>
      <path id="ic-setting-00_svg__a" d="M0 24h24V0H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="ic-setting-00_svg__b" fill="#fff">
        <use xlinkHref="#ic-setting-00_svg__a" />
      </mask>
      <path
        fill={color || 'currentColor'}
        d="M3 6a1 1 0 1 1 0-2h11.185A2.995 2.995 0 0 1 17 2c1.302 0 2.401.839 2.815 2H21a1 1 0 1 1 0 2h-1.185A2.995 2.995 0 0 1 17 8a2.995 2.995 0 0 1-2.815-2H3zm18 5a1 1 0 1 1 0 2H9.816A2.997 2.997 0 0 1 7 15a2.995 2.995 0 0 1-2.815-2H3a1 1 0 1 1 0-2h1.184A2.997 2.997 0 0 1 7 9c1.302 0 2.401.839 2.816 2H21zm-4 9a1 1 0 1 1 0-2.001 1 1 0 0 1 0 2m4-2h-1.185A2.995 2.995 0 0 0 17 16a2.995 2.995 0 0 0-2.815 2H3a1 1 0 1 0 0 2h11.185A2.995 2.995 0 0 0 17 22a2.995 2.995 0 0 0 2.815-2H21a1 1 0 1 0 0-2"
        mask="url(#ic-setting-00_svg__b)"
      />
    </g>
  </svg>
);

SvgIcSetting00.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcSetting00.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcSetting00;
