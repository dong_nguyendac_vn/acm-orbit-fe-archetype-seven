import React from 'react';
import PropTypes from 'prop-types';

const SvgIcProductConfiguration = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <g fill="none" fillRule="evenodd">
      <path d="M0 24h24V0H0z" />
      <g fill={color || 'currentColor'}>
        <path d="M2.5 6.25h11.781A2.998 2.998 0 0 0 17 8c1.207 0 2.243-.72 2.719-1.75H21.5a1.25 1.25 0 0 0 0-2.5h-1.781A2.998 2.998 0 0 0 17 2c-1.207 0-2.243.72-2.719 1.75H2.5a1.25 1.25 0 0 0 0 2.5M2.5 13.25h1.781A3 3 0 0 0 7 15a3 3 0 0 0 2.719-1.75H21.5a1.25 1.25 0 0 0 0-2.5H9.719A2.998 2.998 0 0 0 7 9c-1.207 0-2.243.72-2.719 1.75H2.5a1.25 1.25 0 0 0 0 2.5M2.5 20.25h11.781A3 3 0 0 0 17 22a3 3 0 0 0 2.719-1.75H21.5a1.25 1.25 0 0 0 0-2.5h-1.781A2.998 2.998 0 0 0 17 16c-1.207 0-2.243.72-2.719 1.75H2.5a1.25 1.25 0 0 0 0 2.5" />
      </g>
    </g>
  </svg>
);

SvgIcProductConfiguration.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcProductConfiguration.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcProductConfiguration;
