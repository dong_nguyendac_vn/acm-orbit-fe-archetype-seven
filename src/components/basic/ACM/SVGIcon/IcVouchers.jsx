import React from 'react';
import PropTypes from 'prop-types';

const SvgIcVouchers = ({ color, height, width, viewbox }) => (
  <svg width={width} height={height} viewbox={viewbox}>
    <g fill="none" fillRule="evenodd">
      <path d="M0 24h24V0H0z" />
      <g fill={color || 'currentColor'}>
        <path d="M16.613 5.557V2.444a.5.5 0 0 0-.724-.448l-3.113 1.557a.5.5 0 0 0 0 .895l3.113 1.556a.5.5 0 0 0 .724-.447M7.387 5.557V2.444a.5.5 0 0 1 .724-.448l3.113 1.557a.5.5 0 0 1 0 .895L8.11 6.004a.5.5 0 0 1-.724-.447" />
        <path d="M21.5 3h-3v3H20v12h-6.5V8h-3v10H4V6h1.5V3h-3A1.5 1.5 0 0 0 1 4.5v15A1.5 1.5 0 0 0 2.5 21h19a1.5 1.5 0 0 0 1.5-1.5v-15A1.5 1.5 0 0 0 21.5 3" />
      </g>
    </g>
  </svg>
);

SvgIcVouchers.defaultProps = {
  color: undefined,
  height: 24,
  width: 24,
  viewbox: '0 -3 24 24',
};

SvgIcVouchers.propTypes = {
  color: PropTypes.string,
  height: PropTypes.number,
  width: PropTypes.number,
  viewbox: PropTypes.string,
};

export default SvgIcVouchers;
