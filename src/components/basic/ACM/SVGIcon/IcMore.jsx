import React from 'react';
import PropTypes from 'prop-types';

const SvgIcMore = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <defs>
      <path
        fill={color || 'currentColor'}
        id="ic-more_svg__a"
        d="M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"
      />
      <path id="ic-more_svg__c" d="M0 0h24v24H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="ic-more_svg__b" fill="#fff">
        <use xlinkHref="#ic-more_svg__a" />
      </mask>
      <use fill="#666" xlinkHref="#ic-more_svg__a" />
      <g mask="url(#ic-more_svg__b)">
        <mask id="ic-more_svg__d" fill="#fff">
          <use xlinkHref="#ic-more_svg__c" />
        </mask>
        <g fill={color || 'currentColor'} mask="url(#ic-more_svg__d)">
          <path d="M0 0h24v24H0z" />
        </g>
      </g>
    </g>
  </svg>
);

SvgIcMore.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcMore.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcMore;
