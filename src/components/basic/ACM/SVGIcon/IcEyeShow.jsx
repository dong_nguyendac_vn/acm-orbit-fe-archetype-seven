import React from 'react';
import PropTypes from 'prop-types';

const SvgIcEyeShow = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <defs>
      <path
        fill={color || 'currentColor'}
        id="ic-eye-show_svg__a"
        d="M12 9c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3m0 8c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5m0-12.5C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5"
      />
      <path id="ic-eye-show_svg__c" d="M0 0h24v24H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="ic-eye-show_svg__b" fill="#fff">
        <use xlinkHref="#ic-eye-show_svg__a" />
      </mask>
      <use fill="#666" xlinkHref="#ic-eye-show_svg__a" />
      <g mask="url(#ic-eye-show_svg__b)">
        <mask id="ic-eye-show_svg__d" fill="#fff">
          <use xlinkHref="#ic-eye-show_svg__c" />
        </mask>
        <g fill="none" mask="url(#ic-eye-show_svg__d)">
          <path d="M0 0h24v24H0z" />
        </g>
      </g>
    </g>
  </svg>
);

SvgIcEyeShow.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcEyeShow.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcEyeShow;
