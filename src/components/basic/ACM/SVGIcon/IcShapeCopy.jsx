import React from 'react';
import PropTypes from 'prop-types';

const SvgIcShapeCopy = ({ width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <path
      fill="#B2B2B2"
      fillOpacity={0.5}
      d="M113.461 0H16.54L0 20.15V130h130V20.15L113.461 0zM64.504 105.19L24.809 65.166h25.26V50.611h28.87v14.554h25.26l-39.695 40.026zM14.885 13.894l5.846-6.946h86.607l6.784 6.946H14.885z"
    />
  </svg>
);

SvgIcShapeCopy.defaultProps = {
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcShapeCopy.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcShapeCopy;
