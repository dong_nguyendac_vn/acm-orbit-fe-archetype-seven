import React from 'react';
import PropTypes from 'prop-types';

const SvgIcRefresh = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <defs>
      <path
        fill={color || 'currentColor'}
        id="ic-refresh_svg__a"
        d="M17.355 6.074a7.947 7.947 0 0 0-5.014-2.057C12.227 4.012 12.116 4 12 4c-4.412 0-8 3.589-8 8 0 4.41 3.588 8 8 8a8.023 8.023 0 0 0 7.736-6H17.65a6.016 6.016 0 0 1-4.42 3.872 6.007 6.007 0 0 1-7.102-7.102c.055-.26.13-.511.218-.758A6.022 6.022 0 0 1 12 6a5.97 5.97 0 0 1 3.94 1.487L12.93 10.5H20V3.429l-2.646 2.645z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path fill="transparent" d="M0 24h24V0H0z" />
      <use fill="#666" xlinkHref="#ic-refresh_svg__a" />
    </g>
  </svg>
);

SvgIcRefresh.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcRefresh.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcRefresh;
