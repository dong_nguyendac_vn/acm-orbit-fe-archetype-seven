import React from 'react';
import PropTypes from 'prop-types';

const SvgIcHome = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <g fill="none" fillRule="evenodd">
      <path d="M0 24h24V0H0z" />
      <path
        d="M2 14h1v7a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-7h6v7a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-7h1a.999.999 0 0 0 .707-1.707l-10-10a.999.999 0 0 0-1.414 0l-10 10A1 1 0 0 0 2 14"
        fill={color || 'currentColor'}
      />
    </g>
  </svg>
);

SvgIcHome.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcHome.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcHome;
