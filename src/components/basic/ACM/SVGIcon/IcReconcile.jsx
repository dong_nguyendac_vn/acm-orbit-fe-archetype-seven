import React from 'react';
import PropTypes from 'prop-types';

const SvgIcReconcile = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <g fill="none" fillRule="evenodd">
      <path d="M0 24h24V0H0z" />
      <path
        d="M10 20v-7H4V9.5h4.086l-.414.415a.999.999 0 1 0 1.414 1.414l2.121-2.122a.999.999 0 0 0 0-1.414L9.086 5.672a.999.999 0 1 0-1.414 1.414l.414.414H4V4h10v7h6v3.5h-4.086l.414-.414a.999.999 0 1 0-1.414-1.414l-2.121 2.121a.999.999 0 0 0 0 1.414l2.121 2.122a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414l-.414-.415H20V20H10zM20 8h-3V4a3 3 0 0 0-3-3H4a3 3 0 0 0-3 3v9a3 3 0 0 0 3 3h3v4a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3v-9a3 3 0 0 0-3-3z"
        fill={color || 'currentColor'}
      />
    </g>
  </svg>
);

SvgIcReconcile.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcReconcile.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcReconcile;
