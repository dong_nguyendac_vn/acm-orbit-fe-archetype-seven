import React from 'react';
import PropTypes from 'prop-types';

const SvgIcCampaign = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <g fill="none" fillRule="evenodd">
      <path d="M0 24h24V.001H0z" />
      <path
        d="M17.25 15.823l-2.329-1.165a1.504 1.504 0 0 0-.67-.157H8.996a3.966 3.966 0 0 1-2.6-.989A3.972 3.972 0 0 1 5 10.502c0-2.206 1.794-4 4-4h5.25c.232 0 .463-.056.671-.159l2.33-1.164v10.645zm2.29-14.35c.44.274.71.757.71 1.277v15.5a1.503 1.503 0 0 1-1.5 1.5c-.229 0-.459-.052-.67-.157L13.895 17.5H11.94l.551 3.858a1.003 1.003 0 0 1-.99 1.142h-2a1 1 0 0 1-.919-.607L6.496 17.03a7.027 7.027 0 0 1-4.216-4.585c-.09.026-.18.056-.28.056a1 1 0 0 1-1-1v-2a1 1 0 0 1 1-1c.1 0 .19.03.28.056C3.125 5.641 5.815 3.502 9 3.502h4.896l4.183-2.092a1.506 1.506 0 0 1 1.46.064zm3.019 6.384a4.065 4.065 0 0 1 .001 5.286 1.002 1.002 0 0 1-1.409.124 1 1 0 0 1-.124-1.41c.231-.275.508-.734.508-1.356a2.1 2.1 0 0 0-.507-1.357 1 1 0 1 1 1.531-1.287z"
        fill={color || 'currentColor'}
      />
    </g>
  </svg>
);

SvgIcCampaign.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcCampaign.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcCampaign;
