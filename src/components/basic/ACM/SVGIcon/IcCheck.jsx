import React from 'react';
import PropTypes from 'prop-types';

const SvgIcCheck = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <defs>
      <path
        fill={color || 'currentColor'}
        id="ic-check_svg__a"
        d="M9 16.2l-3.5-3.5a.99.99 0 1 0-1.4 1.4l4.193 4.193a1 1 0 0 0 1.414 0L20.3 7.7a.99.99 0 0 0-1.4-1.4L9 16.2z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <mask id="ic-check_svg__b" fill="#fff">
        <use xlinkHref="#ic-check_svg__a" />
      </mask>
      <use fill="#666" xlinkHref="#ic-check_svg__a" />
      <g fill={color || 'currentColor'} mask="url(#ic-check_svg__b)">
        <path d="M0 0h24v24H0z" />
      </g>
    </g>
  </svg>
);

SvgIcCheck.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcCheck.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcCheck;
