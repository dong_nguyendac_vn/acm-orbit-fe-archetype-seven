import React from 'react';
import PropTypes from 'prop-types';

const SvgIcBalanceAdjustment = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <g fill="none" fillRule="evenodd">
      <path d="M0 24h24V0H0z" />
      <g fill={color || 'currentColor'}>
        <path d="M20 16a2 2 0 0 0-2 2H6a2 2 0 0 0-2-2V8a2 2 0 0 0 2-2h6.597l3-3H2.5A1.5 1.5 0 0 0 1 4.5v15A1.5 1.5 0 0 0 2.5 21h19a1.5 1.5 0 0 0 1.5-1.5V6.91l-3 3V16z" />
        <path d="M21.847 5.234l1.103-1.102-1.103 1.102L20 7.081l-.765.765.765-.765zM11.629 9.796c-.008.008-.01.02-.018.028l-.008.015a.497.497 0 0 0-.095.152l-1.061 3.182c-.021.065-.014.133-.008.198v.002a.54.54 0 0 0 .029.13.475.475 0 0 0 .1.182.502.502 0 0 0 .512.12l3.182-1.06a.497.497 0 0 0 .195-.121l-.353-.353-.396-.396-1.33-1.33-.395-.395-.354-.354z" />
        <path d="M23.466 3.182a.5.5 0 0 0-.11-.164l-.605-.606-.19-.19L21.237.898a.502.502 0 0 0-.708 0L18.425 3l-3 3-2.723 2.723.354.354.353.353 1.414 1.414.354.354.353.353.04-.04 3.665-3.664L20 7.08l1.847-1.846 1.102-1.103.408-.407a.511.511 0 0 0 .109-.164c.015-.036.017-.075.022-.113a.482.482 0 0 0 .002-.142c-.005-.042-.008-.084-.024-.124" />
      </g>
    </g>
  </svg>
);

SvgIcBalanceAdjustment.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcBalanceAdjustment.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcBalanceAdjustment;
