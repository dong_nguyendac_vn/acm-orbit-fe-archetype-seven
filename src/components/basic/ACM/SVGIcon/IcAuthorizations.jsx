import React from 'react';
import PropTypes from 'prop-types';

const SvgIcAuthorizations = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <g fill="none" fillRule="evenodd">
      <path d="M0 24h24V0H0z" />
      <g fill={color || 'currentColor'}>
        <path d="M2.5 22h9.764a6.942 6.942 0 0 1-1.184-3H4V4h8.5v2.5A1.5 1.5 0 0 0 14 8h2.5v3.166A6.986 6.986 0 0 1 18 11c.516 0 1.017.06 1.5.166V6.5c0-.099-.01-.196-.029-.292v-.001a1.559 1.559 0 0 0-.175-.449 1.544 1.544 0 0 0-.235-.319l-4-4a1.535 1.535 0 0 0-.403-.28 1.498 1.498 0 0 0-.298-.109c-.071-.019-.145-.025-.218-.032-.035-.004-.068-.012-.102-.013C14.026 1.004 14.014 1 14 1H2.5A1.5 1.5 0 0 0 1 2.5v18A1.5 1.5 0 0 0 2.5 22" />
        <path d="M18.667 20.296c-.22.22-.508.329-.796.329-.288 0-.576-.11-.796-.33l-2.121-2.12a1.127 1.127 0 0 1 1.592-1.592l1.325 1.326 3.544-3.544A4.973 4.973 0 0 0 18 13a5 5 0 0 0-5 5 5 5 0 0 0 5 5 5 5 0 0 0 5-5c0-.605-.124-1.18-.321-1.716l-4.012 4.012zM7 9a1 1 0 1 0 0 2h3a1 1 0 1 0 0-2H7z" />
      </g>
    </g>
  </svg>
);

SvgIcAuthorizations.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcAuthorizations.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcAuthorizations;
