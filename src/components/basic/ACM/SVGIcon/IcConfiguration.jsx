import React from 'react';
import PropTypes from 'prop-types';

const SvgIcConfiguration = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <g fill="none" fillRule="evenodd">
      <path d="M0 24h24V0H0z" />
      <path
        d="M8.2 12c0-2.096 1.704-3.8 3.8-3.8s3.8 1.704 3.8 3.8-1.704 3.8-3.8 3.8A3.803 3.803 0 0 1 8.2 12M22 9.8h-1.488a8.727 8.727 0 0 0-.938-2.263l1.053-1.053a1 1 0 0 0 0-1.414l-1.698-1.697a.997.997 0 0 0-1.414 0l-1.052 1.053a8.741 8.741 0 0 0-2.263-.938V2a1 1 0 0 0-1-1h-2.4a1 1 0 0 0-1 1v1.488a8.727 8.727 0 0 0-2.263.938L6.484 3.373a.997.997 0 0 0-1.414 0L3.373 5.07a.999.999 0 0 0 0 1.414l1.053 1.053A8.727 8.727 0 0 0 3.488 9.8H2a1 1 0 0 0-1 1v2.4a1 1 0 0 0 1 1h1.488c.208.805.525 1.565.938 2.263l-1.053 1.053a.999.999 0 0 0 0 1.414l1.697 1.697a.997.997 0 0 0 1.414 0l1.053-1.053c.698.413 1.458.73 2.263.938V22a1 1 0 0 0 1 1h2.4a1 1 0 0 0 1-1v-1.488a8.741 8.741 0 0 0 2.263-.938l1.052 1.053a1 1 0 0 0 1.414 0l1.698-1.697a1 1 0 0 0 0-1.414l-1.053-1.053c.413-.698.73-1.458.938-2.263H22a1 1 0 0 0 1-1v-2.4a1 1 0 0 0-1-1"
        fill={color || 'currentColor'}
      />
    </g>
  </svg>
);

SvgIcConfiguration.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcConfiguration.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcConfiguration;
