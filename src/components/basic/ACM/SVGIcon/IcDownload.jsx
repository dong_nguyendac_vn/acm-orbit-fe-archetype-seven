import React from 'react';
import PropTypes from 'prop-types';

const SvgIcDownload = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <defs>
      <path
        fill={color || 'currentColor'}
        id="ic-save_svg__a"
        d="M19 9h-4V3H9v6H5l7 7 7-7zM5 20a1 1 0 0 0 1 1h12a1 1 0 0 0 0-2H6a1 1 0 0 0-1 1z"
      />
    </defs>
    <use fill="#127BD4" fillRule="evenodd" xlinkHref="#ic-save_svg__a" />
  </svg>
);

SvgIcDownload.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcDownload.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcDownload;
