import React from 'react';
import PropTypes from 'prop-types';

const SvgIcAuthentification = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <path
      d="M4.25 16.25C3.561 16.25 3 15.689 3 15s.561-1.25 1.25-1.25c.69 0 1.25.561 1.25 1.25s-.56 1.25-1.25 1.25M7.326 16a3.222 3.222 0 0 1-.326.713 3.268 3.268 0 0 1-1 1.017 3.22 3.22 0 0 1-1.75.52A3.254 3.254 0 0 1 1 15a3.254 3.254 0 0 1 3.25-3.25 3.22 3.22 0 0 1 1.75.52c.405.261.744.608 1 1.017.138.221.244.461.326.713H15a1 1 0 0 1 1 1v3h-2v-2H7.326zM21 8.184A2.99 2.99 0 0 1 23 11v9a3 3 0 0 1-3 3H10a3 3 0 0 1-3-3v-1h3v1h10v-9H7c0-.842.35-1.6.909-2.145a2.99 2.99 0 0 1 1.09-.671A2.966 2.966 0 0 1 10 8H18.5V6.57c0-1.693-1.57-3.07-3.5-3.07-1.656 0-3.041 1.018-3.402 2.375H9.052C9.422 3.13 11.945 1 15 1c3.309 0 6 2.498 6 5.57v1.614z"
      fill={color || 'currentColor'}
      fillRule="evenodd"
    />
  </svg>
);

SvgIcAuthentification.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcAuthentification.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcAuthentification;
