import React from 'react';
import PropTypes from 'prop-types';

const SvgIcClose = ({ color, width, height, viewBox }) => (
  <svg width={width} height={height} viewBox={viewBox}>
    <defs>
      <path
        fill={color || 'currentColor'}
        id="ic-close_svg__a"
        d="M13.3.7a.99.99 0 0 0-1.4 0L7 5.6 2.1.7A.99.99 0 1 0 .7 2.1L5.6 7 2.8 9.8.7 11.9a.99.99 0 0 0 1.4 1.4L7 8.4l4.9 4.9a.99.99 0 0 0 1.4-1.4L8.4 7l4.9-4.9a.99.99 0 0 0 0-1.4z"
      />
    </defs>
    <use fill="#666" fillRule="evenodd" transform="translate(5 5)" xlinkHref="#ic-close_svg__a" />
  </svg>
);

SvgIcClose.defaultProps = {
  color: undefined,
  width: 24,
  height: 24,
  viewBox: '0 -3 24 24',
};

SvgIcClose.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewBox: PropTypes.string,
};

export default SvgIcClose;
