import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './ACMTableContent.module.less';

const ACMTableContent = ({ children, className }) => {
  const clsString = classNames(styles.acm__table__content, className);
  return <div className={clsString}>{children}</div>;
};

ACMTableContent.defaultProps = {
  className: undefined,
  children: undefined,
};

ACMTableContent.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

export default memo(ACMTableContent);
