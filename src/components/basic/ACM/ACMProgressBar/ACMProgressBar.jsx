import React from 'react';
import { Progress } from 'antd';
import PropTypes from 'prop-types';
import './ACMProgressBar.less';

const ACMProgressBar = ({ id, percent }) => {
  return <Progress id={id} percent={percent} />;
};

ACMProgressBar.propTypes = {
  id: PropTypes.string,
  percent: PropTypes.number,
};

ACMProgressBar.defaultProps = {
  id: 'acmProgress',
  percent: 50,
};
export default ACMProgressBar;
