import PropTypes from 'prop-types';
import React, { memo } from 'react';
import ACMButton from 'components/basic/ACM/ACMButton';
import ACMIcon from 'components/basic/ACM/ACMIcon';
import { MoreOutlined, EyeOutlined } from '@ant-design/icons';
import styles from './ACMTableAction.module.less';

const { ActionButton } = ACMButton;
const { ACMIconEdit, ACMIconDelete } = ACMIcon;

const ACMTableAction = ({ view, edit, delete: deleted, more }) => {
  return (
    <div className={styles.acm__table__action}>
      {view && (
        <ActionButton
          type="info"
          tooltip={view.title}
          onClick={view.onClick}
          shape="circle"
          size="small"
          icon={<EyeOutlined />}
        />
      )}
      {edit && (
        <ActionButton
          type="warn"
          tooltip={edit.title}
          shape="circle"
          size="small"
          onClick={edit.onClick}
          icon={<ACMIconEdit />}
        />
      )}
      {deleted && (
        <ActionButton
          type="danger"
          tooltip={deleted.title}
          shape="circle"
          size="small"
          onClick={deleted.onClick}
          icon={<ACMIconDelete />}
        />
      )}
      {more && (
        <ActionButton
          type="gray"
          tooltip={more.title}
          shape="circle"
          size="small"
          onClick={more.onClick}
          icon={<MoreOutlined />}
        />
      )}
    </div>
  );
};
ACMTableAction.defaultProps = {
  view: {
    title: 'View',
    onClick: () => {},
  },
  edit: {
    title: 'Edit',
    onClick: () => {},
  },
  delete: {
    title: 'Delete',
    onClick: () => {},
  },
  more: {
    title: 'More',
    onClick: () => {},
  },
};
ACMTableAction.propTypes = {
  view: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      title: PropTypes.string,
      onClick: PropTypes.func,
    }),
  ]),
  edit: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      title: PropTypes.string,
      onClick: PropTypes.func,
    }),
  ]),
  delete: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      onClick: PropTypes.func,
    }),
  ]),
  more: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      title: PropTypes.string,
      onClick: PropTypes.func,
    }),
  ]),
};

export default memo(ACMTableAction);
