import React, { memo } from 'react';
import { Table } from 'antd';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './ACMTable.module.less';

const rowClass = index => {
  const modClass = index % 2 === 0 ? 'acm__row__even' : 'acm__row__odd';
  const clsString = classNames(modClass);
  return clsString;
};

const defaultPage = {
  pageSize: 50,
  onChange: () => {},
  disabled: false,
  current: 1,
  defaultPageSize: 50,
  defaultCurrent: 1,
};

const ACMTable = ({
  columns,
  rowKey,
  className,
  dataSource,
  pagination,
  loading,
  onRow,
  rowSelection,
}) => {
  const clsString = classNames(styles.acm__table, className);
  const paging = { ...defaultPage, ...pagination };
  return (
    <Table
      onRow={onRow}
      columns={columns}
      rowKey={rowKey}
      rowClassName={(record, index) => rowClass(index)}
      className={clsString}
      dataSource={dataSource}
      pagination={paging}
      rowSelection={rowSelection}
      loading={loading}
    />
  );
};

ACMTable.defaultProps = {
  className: undefined,
  loading: false,
  rowKey: 'key',
  pagination: undefined,
  rowSelection: undefined,
  onRow: undefined,
  dataSource: [],
};
ACMTable.propTypes = {
  rowSelection: PropTypes.shape({
    onSelect: PropTypes.func,
    onChange: PropTypes.func,
  }),
  onRow: PropTypes.func,
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  rowKey: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  className: PropTypes.string,
  dataSource: PropTypes.arrayOf(PropTypes.object),
  pagination: PropTypes.shape({
    pageSize: PropTypes.number,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    current: PropTypes.number,
    defaultPageSize: PropTypes.number,
    defaultCurrent: PropTypes.number,
  }),
  loading: PropTypes.bool,
};

export default memo(ACMTable);
