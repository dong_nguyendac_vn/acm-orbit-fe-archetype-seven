import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ACMRow from 'components/basic/ACM/ACMRow/ACMRow';
import ACMButton from './ACMButton';
import styles from './ACMDataButton.module.less';

const ACMDataButton = ({ type, className, cancel, ok, htmlType }) => {
  const clsString = classNames(styles.acm__modal__actions, className);
  return (
    <ACMRow type="flex" justify="end" className={clsString}>
      {cancel && (
        <ACMButton type="default" className="acm-modal-actions-cancel" onClick={cancel.onClick}>
          {cancel.title}
        </ACMButton>
      )}
      {ok && (
        <ACMButton
          type={type}
          htmlType={htmlType}
          className="acm-modal-actions-ok"
          onClick={ok.onClick}
        >
          {ok.title}
        </ACMButton>
      )}
    </ACMRow>
  );
};

ACMDataButton.defaultProps = {
  type: 'primary',
  htmlType: 'button',
  className: undefined,
  cancel: {
    title: 'Cancel',
    onClick: () => {},
  },
  ok: {
    title: 'Done',
    onClick: () => {},
  },
};

ACMDataButton.propTypes = {
  htmlType: PropTypes.string,
  type: PropTypes.string,
  className: PropTypes.string,
  cancel: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      title: PropTypes.string,
      onClick: PropTypes.func,
    }),
  ]),
  ok: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      title: PropTypes.string,
      onClick: PropTypes.func,
    }),
  ]),
};

export default memo(ACMDataButton);
