import React, { memo } from 'react';
import { Button } from 'antd';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ACMTooltip from 'components/basic/ACM/ACMTooltip/ACMTooltip';
import styles from './ACMButton.module.less';

const ACMButton = ({
  icon: Icon,
  htmlType,
  loading,
  size,
  type,
  children,
  className,
  onClick,
  tooltip,
  shape,
  style,
}) => {
  const clsString = classNames(styles.acm__button, className);
  return (
    <ACMTooltip title={tooltip}>
      <Button
        className={clsString}
        htmlType={htmlType}
        loading={loading}
        onClick={onClick}
        size={size}
        type={type}
        style={style}
        shape={shape}
      >
        {Icon && Icon}
        {children}
      </Button>
    </ACMTooltip>
  );
};

ACMButton.defaultProps = {
  icon: undefined,
  loading: false,
  size: 'default',
  type: 'primary',
  children: undefined,
  htmlType: 'button',
  className: '',
  shape: undefined,
  tooltip: undefined,
  onClick: () => {},
  style: undefined,
};

ACMButton.propTypes = {
  style: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  icon: PropTypes.node,
  loading: PropTypes.bool,
  tooltip: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.func, PropTypes.node]),
  type: PropTypes.string,
  size: PropTypes.string,
  shape: PropTypes.string,
  htmlType: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

export default memo(ACMButton);
