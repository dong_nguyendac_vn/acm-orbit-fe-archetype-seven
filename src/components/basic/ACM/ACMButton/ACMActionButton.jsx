import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ACMButton from './ACMButton';
import styles from './ACMActionButton.module.less';

const ACMActionButton = ({
  icon,
  htmlType,
  loading,
  size,
  shape,
  type,
  className,
  tooltip,
  onClick,
}) => {
  const clssString = classNames(styles.acm__action__button, className);
  return (
    <ACMButton
      htmlType={htmlType}
      className={clssString}
      loading={loading}
      shape={shape}
      size={size}
      type={type}
      tooltip={tooltip}
      onClick={onClick}
      icon={icon}
    />
  );
};

ACMActionButton.defaultProps = {
  icon: undefined,
  loading: false,
  size: 'small',
  shape: undefined,
  type: 'primary',
  className: undefined,
  htmlType: 'button',
  tooltip: '',
  onClick: () => {},
};

ACMActionButton.propTypes = {
  icon: PropTypes.node,
  loading: PropTypes.bool,
  size: PropTypes.string,
  tooltip: PropTypes.string,
  shape: PropTypes.oneOf(['circle-outline', 'circle', 'round']),
  htmlType: PropTypes.string,
  type: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

export default memo(ACMActionButton);
