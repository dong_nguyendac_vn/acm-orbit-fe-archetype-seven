import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Dropdown } from 'antd';

const ACMDropdown = ({
  placement,
  trigger,
  visible,
  disabled,
  onVisibleChange,
  overlay,
  children,
}) => {
  return React.createElement(
    Dropdown,
    {
      placement,
      trigger,
      disabled,
      overlay,
      onVisibleChange,
      ...(visible ? { visible } : {}),
    },
    children,
  );
};

ACMDropdown.defaultProps = {
  placement: 'bottomCenter',
  trigger: ['click', 'hover'],
  visible: undefined,
  disabled: undefined,
  onVisibleChange: undefined,
};

ACMDropdown.propTypes = {
  placement: PropTypes.string,
  onVisibleChange: PropTypes.func,
  visible: PropTypes.bool,
  disabled: PropTypes.bool,
  trigger: PropTypes.arrayOf(PropTypes.string),
  overlay: PropTypes.oneOfType([PropTypes.func, PropTypes.node]).isRequired,
  children: PropTypes.element.isRequired,
};

export default memo(ACMDropdown);
