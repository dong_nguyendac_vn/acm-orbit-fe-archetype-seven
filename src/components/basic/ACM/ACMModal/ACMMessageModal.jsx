import React, { memo } from 'react';
import PropTypes from 'prop-types';
import ACMRow from 'components/basic/ACM/ACMRow/ACMRow';
import ACMCol from 'components/basic/ACM/ACMCol/ACMCol';
import { InfoCircleFilled } from '@ant-design/icons';
import classNames from 'classnames';
import ACMButton from 'components/basic/ACM/ACMButton';
import ACMModal from './ACMModal';
import ACMModalContent from './ACMModalContent';
import styles from './ACMMessageModal.module.less';

const { DataButton } = ACMButton;
const ACMMessageModal = ({ title, message, visible, className, type, cancel, ok }) => {
  const clsString = classNames(styles.acm__message__modal, className);
  return (
    <ACMModal
      onCancel={cancel && cancel.onClick}
      title={title}
      visible={visible}
      className={clsString}
      type={type}
      footer={null}
    >
      <div className="acm-modal-content">
        <ACMRow type="flex" justify="center" align="middle">
          <ACMCol span={3}>
            <InfoCircleFilled className="acm-modal-icon" />
          </ACMCol>
          <ACMCol span={21}>
            <ACMModalContent row={2}>
              <span className="acm-modal-message">{message}</span>
            </ACMModalContent>
          </ACMCol>
        </ACMRow>
      </div>

      <DataButton cancel={cancel} ok={ok} />
    </ACMModal>
  );
};

ACMMessageModal.defaultProps = {
  title: undefined,
  message: undefined,
  visible: false,
  className: undefined,
  type: 'primary',
  cancel: {
    title: 'Cancel',
    onClick: () => {},
  },
  ok: {
    title: 'Done',
    onClick: () => {},
  },
};

ACMMessageModal.propTypes = {
  title: PropTypes.string,
  type: PropTypes.oneOf(['success', 'danger', 'primary', 'warn']),
  visible: PropTypes.bool,
  message: PropTypes.string,
  className: PropTypes.string,
  cancel: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      title: PropTypes.string,
      onClick: PropTypes.func,
    }),
  ]),
  ok: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      title: PropTypes.string,
      onClick: PropTypes.func,
    }),
  ]),
};

export default memo(ACMMessageModal);
