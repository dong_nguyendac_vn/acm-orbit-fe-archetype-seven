import ACMModal from './ACMModal';
import ACMMessageModal from './ACMMessageModal';
import ACMDataModal from './ACMDataModal';

ACMModal.MessageModal = ACMMessageModal;
ACMModal.DataModal = ACMDataModal;
export default ACMModal;
