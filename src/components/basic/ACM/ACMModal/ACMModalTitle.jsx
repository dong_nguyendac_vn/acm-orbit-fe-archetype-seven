import React, { memo } from 'react';
import ACMTypography from 'components/basic/ACM/ACMTypography';
import PropTypes from 'prop-types';

const { ACMTitle } = ACMTypography;

const ACMModalTitle = ({ value }) => {
  return (
    <ACMTitle level={4} ellipsis>
      {value}
    </ACMTitle>
  );
};

ACMModalTitle.defaultProps = {
  value: undefined,
};

ACMModalTitle.propTypes = {
  value: PropTypes.string,
};

export default memo(ACMModalTitle);
