import React, { memo } from 'react';
import ACMTypography from 'components/basic/ACM/ACMTypography';
import PropTypes from 'prop-types';

const { ACMParagraph } = ACMTypography;

const ACMModalContent = ({ children, row }) => {
  return <ACMParagraph ellipsis={{ rows: row }}>{children}</ACMParagraph>;
};

ACMModalContent.defaultProps = {
  children: undefined,
  row: 2,
};

ACMModalContent.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  row: PropTypes.number,
};

export default memo(ACMModalContent);
