import React, { memo } from 'react';
import { Modal } from 'antd';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ACMModalTitle from './ACMModalTitle';
import styles from './ACMModal.module.less';

const ACMModal = props => {
  const {
    title,
    visible,
    onCancel,
    closable,
    destroyOnClose,
    onOk,
    cancelText,
    okText,
    children,
    footer,
    className,
    type,
  } = props;
  const clsString = classNames(styles.acm__modal, `acm-modal-${type}`, className);
  return (
    <Modal
      destroyOnClose={destroyOnClose}
      className={clsString}
      title={<ACMModalTitle value={title} />}
      visible={visible}
      footer={footer}
      cancelText={cancelText}
      okText={okText}
      closable={closable}
      onCancel={onCancel}
      onOk={onOk}
    >
      {children}
    </Modal>
  );
};

ACMModal.defaultProps = {
  title: undefined,
  okText: undefined,
  cancelText: undefined,
  visible: false,
  children: undefined,
  destroyOnClose: true,
  footer: null,
  onCancel: () => {},
  onOk: () => {},
  closable: true,
  className: undefined,
  type: 'primary',
};

ACMModal.propTypes = {
  destroyOnClose: PropTypes.bool,
  title: PropTypes.string,
  closable: PropTypes.bool,
  footer: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  okText: PropTypes.string,
  cancelText: PropTypes.string,
  visible: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  onCancel: PropTypes.func,
  onOk: PropTypes.func,
  type: PropTypes.oneOf(['success', 'danger', 'primary', 'warn']),
  className: PropTypes.string,
};

export default memo(ACMModal);
