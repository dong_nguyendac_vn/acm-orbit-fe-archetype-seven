import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ACMModal from './ACMModal';
import styles from './ACMDataModal.module.less';

const ACMDataModal = ({ title, visible, className, type, onCancel, children, destroyOnClose }) => {
  const clsString = classNames(styles.acm__data__modal, className);
  return (
    <ACMModal
      onCancel={onCancel}
      title={title}
      visible={visible}
      destroyOnClose={destroyOnClose}
      className={clsString}
      type={type}
      footer={null}
    >
      <div className="acm-modal-data-content">{children}</div>
    </ACMModal>
  );
};

ACMDataModal.defaultProps = {
  title: undefined,
  destroyOnClose: undefined,
  visible: false,
  className: undefined,
  type: 'primary',
  onCancel: () => {},
  children: undefined,
};

ACMDataModal.propTypes = {
  destroyOnClose: PropTypes.bool,
  title: PropTypes.string,
  type: PropTypes.oneOf(['success', 'danger', 'primary', 'warn']),
  visible: PropTypes.bool,
  className: PropTypes.string,
  onCancel: PropTypes.func,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.func, PropTypes.node]),
};

export default memo(ACMDataModal);
