import React, { memo } from 'react';
import { Tooltip } from 'antd';
import PropTypes from 'prop-types';

const ACMTooltip = ({ children, overlayClassName, placement, title }) => {
  return (
    <Tooltip placement={placement} title={title} overlayClassName={overlayClassName}>
      {children}
    </Tooltip>
  );
};

ACMTooltip.defaultProps = {
  placement: 'top',
  overlayClassName: undefined,
  title: undefined,
  children: undefined,
};

ACMTooltip.propTypes = {
  placement: PropTypes.oneOf(['top', 'bottom', 'right', 'left']),
  overlayClassName: PropTypes.string,
  title: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

export default memo(ACMTooltip);
