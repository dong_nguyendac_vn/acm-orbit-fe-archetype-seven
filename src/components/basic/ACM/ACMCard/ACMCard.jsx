import React, { memo } from 'react';
import { Card } from 'antd';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './ACMCard.module.less';

const ACMCard = ({
  bordered,
  actions,
  cover,
  hoverable,
  loading,
  style,
  size,
  title,
  type,
  onTabChange,
  children,
  className,
}) => {
  const clsString = classNames(styles.acm__card, className);
  return (
    <Card
      actions={actions}
      style={style}
      bordered={bordered}
      cover={cover}
      hoverable={hoverable}
      loading={loading}
      size={size}
      title={title}
      type={type}
      className={clsString}
      onTabChange={onTabChange}
    >
      {children}
    </Card>
  );
};

ACMCard.propTypes = {
  bordered: PropTypes.bool,
  cover: PropTypes.node,
  hoverable: PropTypes.bool,
  loading: PropTypes.bool,
  style: PropTypes.oneOfType([PropTypes.object]),
  size: PropTypes.oneOf(['default', 'small']),
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  type: PropTypes.string,
  onTabChange: PropTypes.func,
  children: PropTypes.node,
  actions: PropTypes.arrayOf(PropTypes.node),
  className: PropTypes.string,
};

ACMCard.defaultProps = {
  children: undefined,
  hoverable: false,
  cover: undefined,
  actions: [],
  loading: false,
  bordered: false,
  style: undefined,
  size: 'default',
  title: undefined,
  type: undefined,
  onTabChange: () => {},
  className: undefined,
};

export default memo(ACMCard);
