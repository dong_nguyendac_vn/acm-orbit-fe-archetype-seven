import React from 'react';
import { Spin, Icon } from 'antd';
import PropTypes from 'prop-types';
import './ACMLoadingIcon.less';

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

const ACMLoadingIconBlack = ({ id, delay, size }) => {
  return (
    <div className="acm__spin_black">
      <Spin id={id} indicator={antIcon} delay={delay} size={size} />
    </div>
  );
};

const ACMLoadingIconWhite = ({ id, delay, size }) => {
  return (
    <div className="acm__spin_white">
      <Spin id={id} indicator={antIcon} delay={delay} size={size} />
    </div>
  );
};

ACMLoadingIconBlack.propTypes = {
  // indicator: PropTypes.oneOfType([PropTypes.object]),
  delay: PropTypes.number,
  size: PropTypes.string,
  id: PropTypes.string,
};

ACMLoadingIconBlack.defaultProps = {
  id: 'acmSpin',
  // indicator: {},
  delay: 10,
  size: 'small',
};

ACMLoadingIconWhite.propTypes = {
  // indicator: PropTypes.oneOfType([PropTypes.object]),
  delay: PropTypes.number,
  size: PropTypes.string,
  id: PropTypes.string,
};

ACMLoadingIconWhite.defaultProps = {
  id: 'acmSpin',
  // indicator: {},
  delay: 10,
  size: 'small',
};
export { ACMLoadingIconBlack, ACMLoadingIconWhite };
