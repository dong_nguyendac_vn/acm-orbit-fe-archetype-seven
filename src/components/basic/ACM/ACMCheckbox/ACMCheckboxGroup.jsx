import React, { memo } from 'react';
import PropTypes from 'prop-types';
import ACMCheckbox from './ACMCheckbox';

const { Group } = ACMCheckbox;

const ACMCheckboxGroup = ({ id, defaultValue, disabled, name, options, value, onChange }) => {
  return (
    <Group
      id={id}
      defaultValue={defaultValue}
      disabled={disabled}
      name={name}
      options={options}
      value={value}
      onChange={onChange}
    />
  );
};

ACMCheckboxGroup.propTypes = {
  id: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.string),
  disabled: PropTypes.bool,
  name: PropTypes.string,
  onChange: PropTypes.func,
  defaultValue: PropTypes.arrayOf(PropTypes.string),
  value: PropTypes.arrayOf(PropTypes.string),
};

ACMCheckboxGroup.defaultProps = {
  id: undefined,
  name: undefined,
  value: [],
  options: [],
  disabled: false,
  onChange: () => {},
  defaultValue: [],
};
export default memo(ACMCheckboxGroup);
