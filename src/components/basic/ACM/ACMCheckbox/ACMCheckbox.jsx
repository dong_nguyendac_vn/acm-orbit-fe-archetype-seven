import React, { memo } from 'react';
import { Checkbox } from 'antd';
import PropTypes from 'prop-types';

const ACMCheckbox = ({ onChange, disabled, id, checked, children, defaultChecked }) => {
  return (
    <Checkbox
      onChange={onChange}
      disabled={disabled}
      id={id}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...(checked ? { checked } : {})}
      defaultChecked={defaultChecked}
    >
      {children}
    </Checkbox>
  );
};

ACMCheckbox.propTypes = {
  id: PropTypes.string,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  children: PropTypes.node,
  onChange: PropTypes.func,
  defaultChecked: PropTypes.bool,
};

ACMCheckbox.defaultProps = {
  id: undefined,
  checked: undefined,
  disabled: false,
  children: undefined,
  onChange: () => {},
  defaultChecked: false,
};

export default memo(ACMCheckbox);
