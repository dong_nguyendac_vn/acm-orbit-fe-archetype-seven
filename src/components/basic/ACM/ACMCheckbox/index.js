import ACMCheckbox from './ACMCheckbox';
import ACMCheckboxGroup from './ACMCheckboxGroup';

ACMCheckbox.Group = ACMCheckboxGroup;
export default ACMCheckbox;
