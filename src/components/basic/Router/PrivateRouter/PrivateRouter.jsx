import { Redirect, Route } from 'react-router-dom';

import PropTypes from 'prop-types';
import React, { memo } from 'react';

function PrivateRouter({ component: Component, isAuthenticated, to }) {
  return (
    <Route
      render={({ location }) =>
        isAuthenticated ? (
          <Component />
        ) : (
          <Redirect
            to={{
              pathname: to,
              state: { redirect: location.pathname, isAuthenticated },
            }}
          />
        )
      }
    />
  );
}

PrivateRouter.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  to: PropTypes.string,
};

PrivateRouter.defaultProps = {
  to: '/',
};

export default memo(PrivateRouter);
