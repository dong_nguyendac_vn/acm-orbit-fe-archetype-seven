import { Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import React, { memo } from 'react';

function RoutePublic({ component: Component, isAuthenticated, to }) {
  return <Route render={() => (isAuthenticated ? <Redirect to={to} /> : <Component />)} />;
}

RoutePublic.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  to: PropTypes.string,
};

RoutePublic.defaultProps = {
  to: '',
};

export default memo(RoutePublic);
