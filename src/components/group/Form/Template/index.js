import FormTemplateSearch from './Search';
import FormTemplateData from './Data';

export { FormTemplateSearch, FormTemplateData };
