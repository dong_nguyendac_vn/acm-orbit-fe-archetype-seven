import React, { memo } from 'react';
import { ACMInput, ACMForm, ACMButton } from 'components/basic/ACM';
import PropTypes from 'prop-types';

const { Item } = ACMForm;

const { DataButton } = ACMButton;

const FormTemplateData = ({ onFinish, onCancel }) => {
  return (
    <ACMForm
      initialValues={{ code: '', name: '' }}
      layout="vertical"
      autoComplete="off"
      onFinish={onFinish}
    >
      <Item label="Code" name="code" rules={[{ required: true }]}>
        <ACMInput autoFocus />
      </Item>
      <Item label="Name" name="name" rules={[{ required: true }]}>
        <ACMInput />
      </Item>
      <DataButton htmlType="submit" cancel={{ title: 'Cancel', onClick: onCancel }} />
    </ACMForm>
  );
};

FormTemplateData.defaultProps = {
  onFinish: () => {},
  onCancel: () => {},
};
FormTemplateData.propTypes = {
  onFinish: PropTypes.func,
  onCancel: PropTypes.func,
};

export default memo(FormTemplateData);
