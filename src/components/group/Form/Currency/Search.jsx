import React, { memo } from 'react';
import { ACMInput, ACMRow, ACMCol, ACMForm, ACMButton } from 'components/basic/ACM';
import PropTypes from 'prop-types';

const { HorizontalItem } = ACMForm;

const FormCurrencySearch = ({ onFinish }) => {
  return (
    <ACMForm autoComplete="off" onFinish={onFinish}>
      <ACMRow gutter={16}>
        <ACMCol xs={24} md={12}>
          <HorizontalItem label="Code" name="code">
            <ACMInput />
          </HorizontalItem>
        </ACMCol>
        <ACMCol xs={24} md={12}>
          <HorizontalItem label="Name" name="name">
            <ACMInput />
          </HorizontalItem>
        </ACMCol>
      </ACMRow>
      <ACMRow type="flex" jutify="end">
        <ACMCol span={24} push={1}>
          <ACMButton type="primary" style={{ float: 'right' }} htmlType="submit">
            Search
          </ACMButton>
        </ACMCol>
      </ACMRow>
    </ACMForm>
  );
};
FormCurrencySearch.defaultProps = {
  onFinish: () => {},
};
FormCurrencySearch.propTypes = {
  onFinish: PropTypes.func,
};

export default memo(FormCurrencySearch);
