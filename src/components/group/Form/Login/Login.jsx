import React, { memo } from 'react';
import PropTypes from 'prop-types';

const FormLogin = ({ onSubmit }) => {
  return (
    <div>
      <button type="button" onClick={onSubmit}>
        Login
      </button>
    </div>
  );
};

FormLogin.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default memo(FormLogin);
