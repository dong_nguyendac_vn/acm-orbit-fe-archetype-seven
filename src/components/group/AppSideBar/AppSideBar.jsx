import React, { PureComponent } from 'react';
import { Menu, Layout } from 'antd';
import PropTypes from 'prop-types';
import { ACMScrollbar } from 'components/basic/ACM';
import AppSideBarHeader from './components/AppSideBarHeader/AppSideBarHeader';
import AppSideBarMenuItem from './components/AppSideBarMenuItem/AppSideBarMenuItem';
import styles from './AppSideBar.module.less';

const { Sider } = Layout;

class AppSideBar extends PureComponent {
  constructor(props) {
    super(props);
    this.onOpenSubMenu = this.onOpenSubMenu.bind(this);
    this.onMenuClick = this.onMenuClick.bind(this);
    this.onMenuCollapse = this.onMenuCollapse.bind(this);
  }

  onOpenSubMenu(openKeys) {
    const { appOpenMenu } = this.props;
    appOpenMenu({ openKeys });
  }

  onMenuClick(values) {
    const { appSelectMenu } = this.props;
    const { key } = values;
    appSelectMenu({ selected: key });
  }

  onMenuCollapse(collapsed) {
    const { app, appCollapseMenu } = this.props;
    const { collapsedMenu } = app;
    if (collapsed !== collapsedMenu) {
      appCollapseMenu();
    }
  }

  render() {
    const { menus, app } = this.props;
    const { currentMenuKey, openMenuKeys, collapsedMenu } = app;

    return (
      <div className={styles.app_sidebar_container}>
        <Sider
          trigger={null}
          breakpoint="md"
          collapsedWidth="0"
          collapsed={collapsedMenu}
          onCollapse={this.onMenuCollapse}
          width="290"
          className={styles.app_sidebar}
        >
          <AppSideBarHeader collapsed={collapsedMenu} />
          <ACMScrollbar>
            <Menu
              onClick={this.onMenuClick}
              selectedKeys={[currentMenuKey]}
              openKeys={openMenuKeys}
              onOpenChange={this.onOpenSubMenu}
              mode="inline"
              theme="dark"
              className={styles.app__sidebar__menu}
            >
              {menus.map(option => (
                <AppSideBarMenuItem
                  key={option.key}
                  option={option}
                  userPermissions={[]}
                  url="/dashboard"
                />
              ))}
            </Menu>
          </ACMScrollbar>
        </Sider>
      </div>
    );
  }
}

AppSideBar.propTypes = {
  menus: PropTypes.arrayOf(PropTypes.object).isRequired,
  app: PropTypes.shape({
    openMenuKeys: PropTypes.arrayOf(PropTypes.string),
    currentMenuKey: PropTypes.string,
    collapsedMenu: PropTypes.bool,
  }).isRequired,
  appOpenMenu: PropTypes.func.isRequired,
  appSelectMenu: PropTypes.func.isRequired,
  appCollapseMenu: PropTypes.func.isRequired,
};

export default AppSideBar;
