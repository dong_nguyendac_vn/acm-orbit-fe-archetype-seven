import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { ACMLogo } from 'components/basic/ACM';
import styles from './AppSideBarHeader.module.less';

const AppSideBarLogo = ({ collapsed }) => {
  return (
    <div className={styles.app__sidebar__header}>
      <Link to="/" className={styles.app__sidebar__header__link}>
        {!collapsed && <ACMLogo />}
      </Link>
    </div>
  );
};

AppSideBarLogo.defaultProps = {
  collapsed: false,
};
AppSideBarLogo.propTypes = {
  collapsed: PropTypes.bool,
};

export default memo(AppSideBarLogo);
