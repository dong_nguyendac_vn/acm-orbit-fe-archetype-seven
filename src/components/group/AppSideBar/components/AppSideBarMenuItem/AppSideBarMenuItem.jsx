import React, { memo } from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { flatten, checkPermissions } from 'utils/helpers';
import styles from './AppSideBarMenuItem.module.less';

const { Item, SubMenu } = Menu;

const ACMMenuItem = ({
  prefixCls,
  icon: Icon,
  url,
  path,
  label,
  onItemHover,
  onOpenChange,
  onClick,
  eventKey,
  mode,
  index,
  level,
  active,
  expandIcon,
  itemIcon,
  inlineIndent,
}) => {
  return (
    <Item
      mode={mode}
      rootPrefixCls={prefixCls}
      onItemHover={onItemHover}
      onOpenChange={onOpenChange}
      onClick={onClick}
      eventKey={eventKey}
      level={level}
      index={index}
      active={active}
      expandIcon={expandIcon}
      itemIcon={itemIcon}
      inlineIndent={inlineIndent}
    >
      <Link to={`${url}${path}`}>
        <span className="acm-menu-holder">
          {Icon ? <Icon /> : null}
          <span className="acm-menu-text">{label}</span>
        </span>
      </Link>
    </Item>
  );
};

ACMMenuItem.defaultProps = {
  prefixCls: 'ant-menu',
  mode: 'inline',
  icon: undefined,
  url: '/',
  path: '',
  label: '',
  expandIcon: '',
  itemIcon: '',
  onItemHover: undefined,
  onOpenChange: undefined,
  onClick: undefined,
};
ACMMenuItem.propTypes = {
  prefixCls: PropTypes.string,
  icon: PropTypes.oneOfType([PropTypes.func, PropTypes.element]),
  onItemHover: PropTypes.func,
  onOpenChange: PropTypes.func,
  onClick: PropTypes.func,
  url: PropTypes.string,
  path: PropTypes.string,
  label: PropTypes.string,
  eventKey: PropTypes.string.isRequired,
  mode: PropTypes.string,
  inlineIndent: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired,
  active: PropTypes.bool.isRequired,
  expandIcon: PropTypes.string,
  itemIcon: PropTypes.string,
};

const AppSideBarMenuItem = ({
  option,
  url,
  userPermissions,
  parentMenu,
  rootPrefixCls,
  onItemHover,
  onOpenChange,
  onClick,
  eventKey,
  mode,
  index,
  level,
  active,
  expandIcon,
  itemIcon,
  inlineIndent,
}) => {
  const { key, label, icon: Icon, children, path, permission } = option;
  const { values, type } = permission || { values: [], type: 'some' };
  if (children) {
    const childrenPermissions = flatten(children.map(data => data.permission));
    const { values: childrenValues, type: childrenType } = childrenPermissions || {
      values: [],
      type: 'some',
    };
    const permitted = checkPermissions(userPermissions, childrenValues, childrenType);
    if (!permitted) {
      return null;
    }
    return (
      <SubMenu
        mode={mode}
        parentMenu={parentMenu}
        onItemHover={onItemHover}
        onOpenChange={onOpenChange}
        rootPrefixCls={rootPrefixCls}
        eventKey={eventKey}
        className={styles.acm__menu__submenu}
        index={index}
        level={level}
        active={active}
        expandIcon={expandIcon}
        itemIcon={itemIcon}
        inlineIndent={inlineIndent}
        title={
          <span className="op-menu-holder">
            {Icon ? <Icon /> : null}
            <span className="nav-text">{label}</span>
          </span>
        }
      >
        {children.map(child => {
          const { key: keyChild } = child;
          return (
            <AppSideBarMenuItem
              key={keyChild}
              option={child}
              url={url}
              userPermissions={userPermissions}
              parentMenu={parentMenu}
              rootPrefixCls={rootPrefixCls}
              onItemHover={onItemHover}
              onOpenChange={onOpenChange}
              onClick={onClick}
              eventKey={eventKey}
              mode={mode}
              index={index}
              level={level}
              active={active}
              expandIcon={expandIcon}
              itemIcon={itemIcon}
              inlineIndent={inlineIndent}
            />
          );
        })}
      </SubMenu>
    );
  }
  const permitted = checkPermissions(userPermissions, values, type);
  if (!permitted) {
    return null;
  }

  return (
    <ACMMenuItem
      key={key}
      onItemHover={onItemHover}
      onClick={onClick}
      onOpenChange={onOpenChange}
      rootPrefixCls={rootPrefixCls}
      icon={Icon}
      url={url}
      path={path}
      label={label}
      eventKey={eventKey}
      level={level}
      index={index}
      active={active}
      expandIcon={expandIcon}
      itemIcon={itemIcon}
      inlineIndent={inlineIndent}
    />
  );
};

AppSideBarMenuItem.defaultProps = {
  rootPrefixCls: 'ant-menu',
  parentMenu: null,
  mode: 'inline',
  expandIcon: '',
  itemIcon: '',
};

AppSideBarMenuItem.propTypes = {
  option: PropTypes.shape({
    icon: PropTypes.oneOfType([PropTypes.func, PropTypes.element, PropTypes.node]),
    key: PropTypes.string,
    label: PropTypes.string,
    children: PropTypes.arrayOf(PropTypes.object),
    path: PropTypes.string,
    permission: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  url: PropTypes.string.isRequired,
  parentMenu: PropTypes.oneOfType([PropTypes.object, PropTypes.func, PropTypes.string]),
  rootPrefixCls: PropTypes.string,
  userPermissions: PropTypes.arrayOf(PropTypes.string).isRequired,
  onItemHover: PropTypes.func.isRequired,
  onOpenChange: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
  eventKey: PropTypes.string.isRequired,
  mode: PropTypes.string,
  inlineIndent: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired,
  active: PropTypes.bool.isRequired,
  expandIcon: PropTypes.string,
  itemIcon: PropTypes.string,
};

export default memo(AppSideBarMenuItem);
