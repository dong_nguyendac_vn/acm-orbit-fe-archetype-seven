import React, { PureComponent } from 'react';

import { ConnectedRouter } from 'connected-react-router/immutable';
import { PrivateRouter, PublicRouter } from 'components/basic';
import PropTypes from 'prop-types';
import { Switch } from 'react-router';
import history from 'utils/history';
import Exception from 'components/group/Exception/Exception';

class MainApp extends PureComponent {
  render() {
    const { unauthenticatedComponent, dashboardComponent, user, dashboardPrefix } = this.props;

    const { profile } = user;
    const { id } = profile || {};
    const isAuthenticated = id !== undefined;
    return (
      <ConnectedRouter history={history}>
        <Switch>
          <PublicRouter
            exact
            path="/"
            to={dashboardPrefix}
            isAuthenticated={isAuthenticated}
            component={unauthenticatedComponent}
          />
          <PrivateRouter isAuthenticated={isAuthenticated} to="/" component={dashboardComponent} />
          <PublicRouter isAuthenticated={isAuthenticated} component={<Exception type={404} />} />
        </Switch>
      </ConnectedRouter>
    );
  }
}

MainApp.defaultProps = {};

MainApp.propTypes = {
  dashboardComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
  unauthenticatedComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
  user: PropTypes.shape({
    profile: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
  dashboardPrefix: PropTypes.string.isRequired,
};

export default MainApp;
