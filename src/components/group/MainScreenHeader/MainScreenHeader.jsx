import React, { memo } from 'react';
import { ACMTypography } from 'components/basic/ACM';
import PropTypes from 'prop-types';
import { ACMBreadcrumbContainer } from 'containers/basic/ACM';
import classNames from 'classnames';
import styles from './MainScreenHeader.module.less';

const { ACMTitle } = ACMTypography;

const MainScreenHeader = ({ title, className, breadcrumbParam }) => {
  const clsString = classNames(styles.acm__main__header, className);
  return (
    <div className={clsString}>
      <ACMBreadcrumbContainer param={breadcrumbParam} />
      <ACMTitle className={styles.acm__main__header__title} level={3}>
        {title}
      </ACMTitle>
    </div>
  );
};
MainScreenHeader.defaultProps = {
  className: null,
  breadcrumbParam: undefined,
};

MainScreenHeader.propTypes = {
  breadcrumbParam: PropTypes.string,
  title: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default memo(MainScreenHeader);
