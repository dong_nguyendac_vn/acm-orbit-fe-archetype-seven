import React, { PureComponent } from 'react';
import { ACMCard, ACMIcon, ACMScrollbar, ACMTooltip } from 'components/basic/ACM';
import PropTypes from 'prop-types';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { coy } from 'react-syntax-highlighter/dist/esm/styles/prism';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { CopyOutlined } from '@ant-design/icons';
import classNames from 'classnames';

const { ACMIconCodeExpanded, ACMIconCodeCollapsed } = ACMIcon;

const initialState = {
  expanded: false,
};
class MainTemplateCard extends PureComponent {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  expandCollapseCode = () => {
    this.setState(prevState => ({
      expanded: !prevState.expanded,
    }));
  };

  render() {
    const { expanded } = this.state;
    const { children, code, title, className } = this.props;
    const clsString = classNames('acm-components-screen', className);
    return (
      <div className={clsString}>
        <ACMCard
          title={title}
          actions={[
            <ACMTooltip title="Copy">
              <CopyToClipboard text={code}>
                <CopyOutlined />
              </CopyToClipboard>
            </ACMTooltip>,
            expanded ? (
              <ACMTooltip title="Hide Code">
                <span>
                  <ACMIconCodeExpanded onClick={this.expandCollapseCode} />
                </span>
              </ACMTooltip>
            ) : (
              <ACMTooltip title="Show Code">
                <span>
                  <ACMIconCodeCollapsed onClick={this.expandCollapseCode} />
                </span>
              </ACMTooltip>
            ),
          ]}
        >
          {children}
        </ACMCard>
        {expanded && (
          <ACMCard style={{ borderRadius: '0px' }}>
            <ACMScrollbar>
              <SyntaxHighlighter language="jsx" style={coy}>
                {code}
              </SyntaxHighlighter>
            </ACMScrollbar>
          </ACMCard>
        )}
      </div>
    );
  }
}

MainTemplateCard.defaultProps = {
  code: null,
  title: null,
  className: null,
};

MainTemplateCard.propTypes = {
  children: PropTypes.node.isRequired,
  code: PropTypes.string,
  title: PropTypes.string,
  className: PropTypes.string,
};

export default MainTemplateCard;
