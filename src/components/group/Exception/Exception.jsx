import React, { memo } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { ACMButton } from 'components/basic/ACM';
import { DASHBOARD_ROUTE_PREFIX, EXCEPTION_TYPE_CONFIG } from 'configs/config';
import styles from './Exception.module.less';
import { ReactComponent as ExceptionImage } from './exception.svg';

const Exception = ({ className, type, title, description, style }) => {
  const clsString = classNames(styles.acm__exception, className);
  const typeContent = EXCEPTION_TYPE_CONFIG[type];
  const { description: descriptionType, title: titleType } = typeContent;

  return (
    <div className={clsString} style={style}>
      <div className={styles.acm__exception__image__wrapper}>
        <ExceptionImage className={styles.acm__exception__img} />
      </div>
      <div className={styles.acm__exception__content}>
        <h1>{titleType || title}</h1>
        <div className={styles.acm__exception__content__description}>
          {descriptionType || description}
        </div>
        <div className={styles.acm__exception__content__actions}>
          <Link to={`${DASHBOARD_ROUTE_PREFIX}`}>
            <ACMButton type="primary">Back to home</ACMButton>
          </Link>
        </div>
      </div>
    </div>
  );
};

Exception.defaultProps = {
  className: null,
  type: 500,
  title: '500',
  style: {},
  description: 'Sorry, the server is wrong.',
};

Exception.propTypes = {
  className: PropTypes.string,
  type: PropTypes.number,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  title: PropTypes.string,
  description: PropTypes.string,
};

export default memo(Exception);
