import React, { PureComponent } from 'react';
import { Layout } from 'antd';

import PropTypes from 'prop-types';
import AppHeader from 'components/group/AppHeader/AppHeader';
import AppSideBar from 'components/group/AppSideBar/AppSideBar';
import { ACMScrollbar } from 'components/basic/ACM';
import { DashboardRouter } from './components/DashboardRouter';
import styles from './Dashboard.module.less';

const { Content } = Layout;

class DashBoard extends PureComponent {
  render() {
    const {
      app,
      user,
      menus,
      appOpenMenu,
      appSelectMenu,
      prefixRoute,
      dashboardRoutes,
      logOut,
      collapseMenu,
    } = this.props;
    return (
      <Layout style={{ height: '100vh', overflow: 'hidden' }} hasSider>
        <AppSideBar
          app={app}
          menus={menus}
          appOpenMenu={appOpenMenu}
          appSelectMenu={appSelectMenu}
          appCollapseMenu={collapseMenu}
        />
        <Layout>
          <AppHeader app={app} user={user} logOut={logOut} collapseMenu={collapseMenu} />
          <Content
            style={{
              flexShrink: '1',
              position: 'relative',
            }}
          >
            <ACMScrollbar>
              <div className={styles.acm__app__content}>
                <DashboardRouter
                  userPermissions={[]}
                  prefixRoute={prefixRoute}
                  routes={dashboardRoutes}
                />
              </div>
            </ACMScrollbar>
          </Content>
        </Layout>
      </Layout>
    );
  }
}
DashBoard.propTypes = {
  app: PropTypes.shape({
    collapsed: PropTypes.bool,
  }).isRequired,
  user: PropTypes.shape({
    profile: PropTypes.shape({ id: PropTypes.string, userName: PropTypes.string }),
  }).isRequired,
  menus: PropTypes.arrayOf(PropTypes.object).isRequired,
  appOpenMenu: PropTypes.func.isRequired,
  appSelectMenu: PropTypes.func.isRequired,
  logOut: PropTypes.func.isRequired,
  collapseMenu: PropTypes.func.isRequired,
  dashboardRoutes: PropTypes.arrayOf(PropTypes.object).isRequired,
  prefixRoute: PropTypes.string.isRequired,
};

export default DashBoard;
