import React, { Suspense } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router/immutable';
import PropTypes from 'prop-types';
import { Switch } from 'react-router';
import PageLoading from 'components/group/PageLoading';
// import { checkPermissions } from 'utils/helpers';
import Exception from 'components/group/Exception/Exception';
import history from 'utils/history';

export function DashboardRouter(props) {
  const { prefixRoute, routes } = props;
  return (
    <ConnectedRouter history={history}>
      <Suspense fallback={<PageLoading />}>
        <Switch>
          {routes.map(route => {
            const { path, exact, component: LazyComponent } = route;
            // const { values, type } = permission || { values: [], type: 'some' };
            const permited = true;
            return (
              <Route
                exact={exact}
                key={path}
                path={`${prefixRoute}${path}`}
                render={() =>
                  !permited ? (
                    <Redirect
                      key={path}
                      path={`${prefixRoute}${path}`}
                      to={{
                        pathname: `${prefixRoute}/404`,
                      }}
                    />
                  ) : (
                    <LazyComponent />
                  )
                }
              />
            );
          })}
          <Route>
            <Exception type={404} />
          </Route>
        </Switch>
      </Suspense>
    </ConnectedRouter>
  );
}

DashboardRouter.defaultProps = {
  prefixRoute: '',
};

DashboardRouter.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.object).isRequired,
  // userPermissions: PropTypes.arrayOf(PropTypes.string).isRequired,
  prefixRoute: PropTypes.string,
};

export default DashboardRouter;
