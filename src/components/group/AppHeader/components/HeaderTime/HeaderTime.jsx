import React, { PureComponent } from 'react';

import moment from 'moment-timezone';
import styles from './HeaderTime.module.less';

class HeaderTime extends PureComponent {
  constructor(props) {
    super(props);
    const tz = moment.tz.guess();
    this.state = {
      currentDateTime: moment.tz(tz),
      timezone: tz,
    };
  }

  componentDidMount() {
    const { timezone } = this.state;
    setInterval(() => {
      this.setState({
        currentDateTime: moment.tz(timezone),
      });
    }, 1000);
  }

  render() {
    const { currentDateTime, timezone } = this.state;
    return (
      <span className={styles.acm__header__time__container}>
        <span className={styles.acm__header__timezone}>{`(UTC ${currentDateTime.format(
          'Z',
        )}) ${timezone}`}</span>
        <span className={styles.acm__header__timezone}>{`${currentDateTime.format(
          'MMM Do YYYY',
        )} | ${currentDateTime.format('hh:mm:ss A')}`}</span>
      </span>
    );
  }
}

export default HeaderTime;
