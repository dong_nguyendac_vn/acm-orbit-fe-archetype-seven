import { Avatar } from 'antd';
import React, { memo, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { UserOutlined } from '@ant-design/icons';
import styles from './HeaderUser.module.less';

const HeaderUser = forwardRef(({ onMouseEnter, userName }, ref) => {
  return (
    <span ref={ref} className={`${styles.app__header__user}`} onMouseEnter={onMouseEnter}>
      <Avatar className={styles.app__header__avatar} size="large" icon={<UserOutlined />} />
      <span className={styles.app__header__name}>{userName}</span>
    </span>
  );
});

HeaderUser.defaultProps = {
  onMouseEnter: () => {},
};

HeaderUser.propTypes = {
  onMouseEnter: PropTypes.func,
  userName: PropTypes.string.isRequired,
};

export default memo(HeaderUser);
