import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu } from 'antd';
import { ACMDropdown, ACMDivider } from 'components/basic/ACM';
import { MenuUnfoldOutlined, MenuFoldOutlined } from '@ant-design/icons';
import HeaderUser from './components/HeaderUser/HeaderUser';
import HeaderTime from './components/HeaderTime/HeaderTime';
import styles from './AppHeader.module.less';

const initialState = {};

const { Item } = Menu;

const { Header } = Layout;

class AppHeader extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = initialState;
    this.onMenuClick = this.onMenuClick.bind(this);
    this.onTriggerButtonClick = this.onTriggerButtonClick.bind(this);
  }

  onMenuClick(values) {
    const { logOut } = this.props;
    const { key } = values;
    switch (key) {
      case 'logout':
        logOut();
        break;
      default:
        break;
    }
  }

  onTriggerButtonClick() {
    const { collapseMenu } = this.props;
    collapseMenu();
  }

  render() {
    const { app, user } = this.props;
    const { collapsedMenu } = app;
    const { profile } = user;
    const { userName } = profile;
    const menu = (
      <Menu selectedKeys={[]} onClick={this.onMenuClick}>
        <Item key="change_password">Change Password</Item>
        <Item key="logout">Logout</Item>
      </Menu>
    );

    return (
      <Header className={styles.app__header}>
        <div className={styles.app__header__trigger}>
          {collapsedMenu ? (
            <MenuUnfoldOutlined
              className={styles.app_header_trigger_button}
              onClick={this.onTriggerButtonClick}
            />
          ) : (
            <MenuFoldOutlined
              className={styles.app_header_trigger_button}
              onClick={this.onTriggerButtonClick}
            />
          )}
        </div>
        <div
          className={`${styles.app__header__user__content} ${
            !collapsedMenu ? `${styles.menu__collapsed}` : ''
          }`}
        >
          <HeaderTime />
          <ACMDivider type="vertical" style={{ height: '70%' }} />
          <ACMDropdown overlay={menu}>
            <HeaderUser userName={userName} />
          </ACMDropdown>
        </div>
      </Header>
    );
  }
}
AppHeader.propTypes = {
  app: PropTypes.shape({
    collapsedMenu: PropTypes.bool,
  }).isRequired,
  user: PropTypes.shape({
    profile: PropTypes.shape({ id: PropTypes.string, userName: PropTypes.string }),
  }).isRequired,
  logOut: PropTypes.func.isRequired,
  collapseMenu: PropTypes.func.isRequired,
};

export default AppHeader;
