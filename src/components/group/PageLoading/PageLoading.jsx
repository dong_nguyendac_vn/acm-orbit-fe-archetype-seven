import React, { useEffect, memo } from 'react';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { ACMRow, ACMCol } from 'components/basic/ACM';

const PageLoading = () => {
  useEffect(() => {
    NProgress.start();
    return () => {
      NProgress.done();
    };
  }, []);

  return (
    <ACMRow>
      <ACMCol span={12} offset={6}>
        Loading...
      </ACMCol>
    </ACMRow>
  );
};

export default memo(PageLoading);
