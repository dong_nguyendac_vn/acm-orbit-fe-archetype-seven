import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { ACMTable } from 'components/basic/ACM';

const { TableAction } = ACMTable;

const initialColumns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: text => {
      return <div>{text}</div>;
    },
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
];
const actionColumns = {
  title: 'Action',
  key: 'action',
  width: 200,
  align: 'center',
  fixed: 'right',
};
const TableTemplatePage = ({ dataSource, onEdit, onView }) => {
  const columns = [...initialColumns];
  actionColumns.render = () => {
    return (
      <TableAction
        edit={{ title: 'Edit', onClick: onEdit }}
        view={{ title: 'View', onClick: onView }}
        more={false}
        delete={false}
      />
    );
  };
  columns.push(actionColumns);
  return <ACMTable columns={columns} dataSource={dataSource} />;
};

TableTemplatePage.defaultProps = {
  dataSource: [],
  onEdit: () => {},
  onView: () => {},
};

TableTemplatePage.propTypes = {
  onEdit: PropTypes.func,
  onView: PropTypes.func,
  dataSource: PropTypes.arrayOf(PropTypes.object),
};

export default memo(TableTemplatePage);
