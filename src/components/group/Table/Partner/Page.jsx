import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { ACMTable } from 'components/basic/ACM';

const { TableAction } = ACMTable;

const initialColumns = [
  {
    title: 'Code',
    dataIndex: 'code',
    key: 'code',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Created',
    dataIndex: 'created_at',
    key: 'last_modified_at',
  },
  {
    title: 'Last modified',
    dataIndex: 'last_modified_at',
    key: 'last_modified_at',
  },
];

const actionColumns = {
  title: 'Action',
  key: 'action',
  width: 200,
  align: 'center',
  fixed: 'right',
};

const TablePartnerPage = ({ dataSource, onEdit, onView, page }) => {
  const columns = [...initialColumns];
  actionColumns.render = () => {
    return (
      <TableAction
        edit={{ title: 'Edit', onClick: onEdit }}
        view={{ title: 'View', onClick: onView }}
        more={false}
        delete={false}
      />
    );
  };
  columns.push(actionColumns);
  return <ACMTable columns={columns} pagination={page} dataSource={dataSource} />;
};

TablePartnerPage.defaultProps = {
  dataSource: [],
  onEdit: () => {},
  onView: () => {},
};

TablePartnerPage.propTypes = {
  onEdit: PropTypes.func,
  onView: PropTypes.func,
  dataSource: PropTypes.arrayOf(PropTypes.object),
  page: PropTypes.shape({
    total: PropTypes.number,
    current: PropTypes.number,
  }).isRequired,
};

export default memo(TablePartnerPage);
