import React, { memo } from 'react';
import { ACMTable } from 'components/basic/ACM';
import PropTypes from 'prop-types';

const columns = [
  {
    title: 'Code',
    dataIndex: 'code',
    key: 'code',
    columnType: 'text',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    columnType: 'text',
  },
];

const TableCurrencyModal = ({ dataSource, onPopupCallBack }) => {
  return (
    <ACMTable
      onRow={(record, rowIndex) => {
        return {
          onClick: () => {
            onPopupCallBack(record, rowIndex);
          },
        };
      }}
      rowKey="code"
      columns={columns}
      dataSource={dataSource}
    />
  );
};

TableCurrencyModal.defaultProps = {
  onPopupCallBack: () => {},
  dataSource: [],
};

TableCurrencyModal.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.object),
  onPopupCallBack: PropTypes.func,
};

export default memo(TableCurrencyModal);
