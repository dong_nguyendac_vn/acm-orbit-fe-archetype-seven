import React, { PureComponent } from 'react';
import { ACMSpin } from 'components/basic/ACM';
import PropTypes from 'prop-types';
import styles from './AppLoading.module.less';

class AppLoading extends PureComponent {
  componentDidMount() {
    const { userLoadInfo } = this.props;
    userLoadInfo();
  }

  render() {
    return (
      <div className={styles.acm__app__loading__container}>
        <ACMSpin spinning size="large" />
      </div>
    );
  }
}

AppLoading.propTypes = {
  userLoadInfo: PropTypes.func.isRequired,
};

export default AppLoading;
