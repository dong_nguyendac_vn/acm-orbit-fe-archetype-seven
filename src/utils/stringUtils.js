const ELLIPSIS = '...';

const isString = value => toString.call(value) === '[object String]';
// Consider using _.truncate(string, {length: maxLength})
// Typo: existy -> exists
const existy = value => value !== undefined && value !== null;

const isEmptyString = value => isString(value) && !value.trim();

const isEmpty = value => (isString(value) ? isEmptyString(value) : !existy(value));

const truncateString = (rawString, maxLength) => {
  if (!isString(rawString) || existy(maxLength)) {
    return rawString;
  }
  return rawString.trim().length > maxLength
    ? rawString
        .trim()
        .substr(0, maxLength)
        .concat(ELLIPSIS)
    : rawString;
};

export { truncateString, isString, isEmptyString, isEmpty };
