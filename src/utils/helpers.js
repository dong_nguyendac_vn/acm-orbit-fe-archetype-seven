// @flow

const checkPermissions = (
  userPermissions: Array<string>,
  allowedPermissions: Array<string>,
  type: string,
): boolean => {
  if (!allowedPermissions || allowedPermissions.length <= 0) {
    return true;
  }
  if (!type || type === 'some') {
    return allowedPermissions.some(permission => userPermissions.includes(permission));
  }
  return allowedPermissions.every(permission => userPermissions.includes(permission));
};

const flatten = (list: Array<any>) =>
  list.reduce((a: any, b: any) => a.concat(Array.isArray(b) ? flatten(b) : b), []);

export { checkPermissions, flatten };
