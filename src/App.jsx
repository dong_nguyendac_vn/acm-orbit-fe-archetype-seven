import React from 'react';
import { DashboardContainer, MainAppContainer, AppLoadingContainer } from 'containers/group';
import { KeycloakWrapperContainer } from 'containers/basic';
import { DASHBOARD_ROUTE_PREFIX } from 'configs/config';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { Provider } from 'react-redux';
import configureStore from 'redux/store';
import PropTypes from 'prop-types';

const { persistor, store } = configureStore();
const MainApp = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <MainAppContainer
          dashboardComponent={() => <DashboardContainer prefixRoute={DASHBOARD_ROUTE_PREFIX} />}
          unauthenticatedComponent={AppLoadingContainer}
          dashboardPrefix={DASHBOARD_ROUTE_PREFIX}
        />
      </PersistGate>
    </Provider>
  );
};
const App = ({ enviroment }) => {
  if (enviroment !== 'development') return <MainApp />;
  return (
    <KeycloakWrapperContainer>
      <MainApp />
    </KeycloakWrapperContainer>
  );
};
App.defaultProps = {
  enviroment: 'development',
};
App.propTypes = {
  enviroment: PropTypes.string,
};

export default App;
